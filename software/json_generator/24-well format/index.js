angular
  .module('application', [])
  .controller('ApplicationController', ApplicationController);

  function ApplicationController() {
    const aCtrl = this;
    aCtrl.title = 'Title';
    aCtrl.loaded = true;
    aCtrl.selectedWells = [];
    aCtrl.cleanedConditionsList = {
      Groups: []
    };

    aCtrl.removeCondition = function(condition) {
      aCtrl.conditionsList.splice(aCtrl.conditionsList.indexOf(condition),1);
    }

    aCtrl.download = function (text, name, type) {
        var a = document.createElement("a");
        var file = new Blob([text], {type: type});
        a.href = URL.createObjectURL(file);
        a.download = name;
        a.click();
    }

    aCtrl.cleanConditionsList = function() {
      aCtrl.cleanedConditionsList = {
        Groups: []
      };
      for (let condition of aCtrl.conditionsList) {
        condition.cleanedWells = [];
        for (let well of condition.selectedWells) {
          condition.cleanedWells.push(well.value);
        }
      }

      for (let condition of aCtrl.conditionsList) {
        if (condition.conditionName !== '') {
          aCtrl.cleanedConditionsList.Groups.push({
            name: condition.conditionName,
            start_time: parseInt(condition.startDelay),
            duration: parseInt(condition.duration),
            irradiance: parseInt(condition.irradianceIntensity),
            cycles: parseInt(condition.numberOfCycles),
            offtime: parseInt(condition.offTime),
            Channels: condition.cleanedWells

          })
        }
      }
    }

    aCtrl.printJsonFile = function() {
      aCtrl.cleanConditionsList();
      console.log(aCtrl.cleanedConditionsList);
      aCtrl.download(JSON.stringify(aCtrl.cleanedConditionsList, null, '\t'), aCtrl.fileName + '.json', 'text/plain');
    }

    aCtrl.generateRandomColor = function() {
      let hex = '#' + ("000000" + Math.random().toString(16).slice(2, 8).toUpperCase()).slice(-6);
      let rgb = hexToRgb(hex);
      return rgb;
    }

    function hexToRgb(hex) {
      var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
      return result ? 'rgba(' + parseInt(result[1], 16) + ',' + parseInt(result[2], 16) + ',' + parseInt(result[3], 16) + ',1)' : null ;
    }

    function rgbToHex(red, green, blue) {
      var rgb = blue | (green << 8) | (red << 16);
      return '#' + (0x1000000 + rgb).toString(16).slice(1)
    }

    aCtrl.getContrast50 = function (hexColor, rgbColor){
      if (rgbColor) {
        hexColor = rgbToHex(rgbColor.split('(')[1].split(',')[0], rgbColor.split('(')[1].split(',')[1], rgbColor.split('(')[1].split(',')[2].split(')')[0]);
      }
      return (parseInt(hexColor.split('#')[1], 16) > 0xffffff/2) ? '#000':'#FFF';
    }

    aCtrl.addCondition = function() {
      let newCondition = {
        color: aCtrl.generateRandomColor(),
        conditionName: '',
        selectedWells: [],
        irradianceIntensity: null,
        duration: null,
        startDelay: null,
        numberOfCycles: null,
        offTime: null
      };
      aCtrl.conditionsList.push(newCondition);
      aCtrl.selectedCondition = aCtrl.conditionsList[aCtrl.conditionsList.length - 1];
      setTimeout(function() {
        componentHandler.upgradeDom();
      },100);
    }

    aCtrl.addWellToCondition = function(well) {
      if (aCtrl.selectedCondition.selectedWells.indexOf(well) === -1) {
        well.conditionsList.push(aCtrl.selectedCondition);
        aCtrl.selectedCondition.selectedWells.push(well)
        var tooltip = $('#' + well.name + '-selection-list');
      }
    }

    aCtrl.selectCondition = function(condition) {
      aCtrl.selectedCondition = condition;
    }

    aCtrl.removeWellFromCondition = function(condition, well) {
      $('#' + well.name).css({
          backgroundColor: 'rgb(255,215,64)',
          color:'#000'
        });

      condition.selectedWells.splice(condition.selectedWells.indexOf(well), 1);
      well.conditionsList.splice(well.conditionsList.indexOf(condition), 1);
      componentHandler.upgradeDom()
    }

    aCtrl.conditionsList = [{
      color: aCtrl.generateRandomColor(),
      conditionName: '',
      selectedWells: [],
      irradianceIntensity: null,
      duration: null,
      startDelay: null,
      numberOfCycles: null,
      offTime: null
    }];

    aCtrl.selectedCondition = aCtrl.conditionsList[0];

    aCtrl.rows = [{
      header: 'A',
      rowStart: 0,
      rowEnd: 6
    },{
      header: 'B',
      rowStart: 6,
      rowEnd: 12
    },{
      header: 'C',
      rowStart: 12,
      rowEnd: 18
    },{
      header: 'D',
      rowStart: 18,
      rowEnd: 24

    }];
    aCtrl.iterations = [1,2,3,4,5,6];//,7,8,9,10,11,12];

    aCtrl.wells = [];

    aCtrl.generateWells = function() {
      let counter = 1;
      for (row of aCtrl.rows) {
        for (index of aCtrl.iterations) {
          aCtrl.wells.push({
            name: row.header + index,
            value: counter,
            conditionsList: []
          });
          counter++;
        }
      }
    }
    aCtrl.generateWells();
  }
