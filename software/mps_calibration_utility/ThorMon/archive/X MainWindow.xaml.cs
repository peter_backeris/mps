﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO.Ports;
using Thorlabs.PM100D_32.Interop;
using System.Threading;
using System.ComponentModel;

namespace ThorMon
{
  
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        bool well24_96 = false;
        SerialPort ComPort;
        PM100D pm;
        bool IsConnected = false;
        String PortName;
        double[] measurements = new double[63];

        private readonly BackgroundWorker bw = new BackgroundWorker();
        public MainWindow()
        {
            InitializeComponent();
            bw.WorkerReportsProgress = true;
           
            bw.ProgressChanged += Bw_ProgressChanged;
            bw.RunWorkerCompleted += Bw_RunWorkerCompleted;
            bw.DoWork += Bw_DoWork;
        }

        private void Bw_DoWork(object sender, DoWorkEventArgs e)
        {
            
           e.Result= btnCalibrate_Click(null, null);
        }

        private void Bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
          
            if ((bool) e.Result)   DrawPoints(measurements);
            btnCalibrate.IsEnabled = true;
        }

        private void Bw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            ProgressBarMain.Value = e.ProgressPercentage;
        }
        private void btnCalibrateStart(object sender, RoutedEventArgs e)
        {
            Graph.Children.Clear();
            Array.Clear(measurements, 0, measurements.Length);
            ProgressBarMain.Value = 0; btnCalibrate.IsEnabled = false;
            if ((bool)well24.IsChecked) well24_96 = true;
            else well24_96 = false;
            bw.RunWorkerAsync();// start thread
        }
        private void btnConnect_Click(object sender, RoutedEventArgs e)
        {
            // Connect  COM port
            checkConnected.IsChecked = false;
            try
            {
                if (cbPorts.SelectedIndex == -1) return;
                PortName =(string) cbPorts.SelectedItem;
                if (ComPort==null) ComPort = new SerialPort(PortName);
                if (IsConnected) { ComPort.Close(); IsConnected = false; }
                ComPort.BaudRate = 57600;
                ComPort.Open();
                ComPort.DataReceived += ComPort_DataReceived;
            }
            catch (Exception ex)
            {
                IsConnected = false;
                return;
            }
            IsConnected = true;
            checkConnected.IsChecked = true;
            ComPort.Write("b0"); //set all channels to dot-correction value 0
            ComPort.Write("a4095"); //set all channels to grayscale value of 4095 (max)
        }

        private void ComPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            string indata = ComPort.ReadExisting();
            
             App.Current.Dispatcher.Invoke(delegate {
                 ReceivedInfo.Text = indata + ReceivedInfo.Text;
             });
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (IsConnected)
                {
                if (ComPort != null) ComPort.Close();
            }

        }
        private void Rescan()
        {
            String[] s = SerialPort.GetPortNames();
            if (s != null)
            {
                foreach (string t in s)
                    cbPorts.Items.Add(t);
            }
            if (cbPorts.Items.Count != 0) cbPorts.SelectedIndex = 0;
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Rescan();
            int j = 0;
            for (j = 1; j <= 96; j++) cbChnNum.Items.Add(j.ToString());
            try
            {
                pm = new PM100D("USB0::0x1313::0x8072::P2006758::INSTR", true, true);
            }
            catch (Exception ex)
            {

            }
        
           
        }

        private void btnRescan_Click(object sender, RoutedEventArgs e)
        {
            Rescan();
        }

        private bool btnCalibrate_Click(object sender, RoutedEventArgs e)
        {
            

            if (!IsConnected) return false;
            if (ComPort == null) return false;
           
            double coff = 1;
            try
            {
               
              

                if (well24_96)
                {
                    coff = 0.636;
                }
                else
                {
                    coff = 0.159;
                }
                
                int startvalue = 0;
                int endvalue = 63;
                Dispatcher.BeginInvoke(new Action(() => {
                    Int32.TryParse(startVal.Text, out startvalue);
                    Int32.TryParse(endVal.Text, out endvalue);
                }));
                
                int len = endvalue - startvalue + 1;
                int j = startvalue;
                int i = 0;
                measurements = new double[len];
                ComPort.Write("p4095");
                for (j = startvalue; j <= endvalue; j++)
                { //measure power for each dot-correction value 0 through 63
                    ComPort.Write("d" + j.ToString()); //set dot-correction value of current channel
                    ComPort.Write("u"); //update channel to change to dot-correction setting
                    Thread.Sleep(200); //delay 100ms to allow system to change
                    pm.measPower(out measurements[i]);//read in value from power meter and store in array
                    measurements[i] *= 1000;
                    measurements[i] /= coff;
                    bw.ReportProgress(i + 1);
                    i++;
                }
                ComPort.Write("d0");
                ComPort.Write("u");

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return (false);
            }

            return (true);
        }
        private void DrawPoints(double[] measurements)
        {
           

            float d = (float)measurements.Max();
            float stepx =(float)(( Graph.ActualWidth-5) / (measurements.Count()-1));
            float startx = 2;
            float stepy = (float)(Graph.ActualHeight - 5) / d;
            float starty = 5;
            List<XYPoint> points = new List<XYPoint>();
            List<XYPoint> points2 = new List<XYPoint>();
            int j = 0;
            foreach (double dp in measurements)
            {
               Rectangle el1 = new Rectangle();
                el1.Height = 5;
                el1.Width = 5;
                el1.Fill= new SolidColorBrush(Color.FromArgb(255, 0, 0, 255));
                Graph.Children.Add(el1);
                Canvas.SetLeft(el1, startx);
                Canvas.SetTop(el1, starty+ stepy * dp);
                points.Add(new XYPoint(startx, starty + stepy * dp));
                points2.Add(new XYPoint(j, dp));
                startx += stepx;
                j++;
            }
            double a, b;
            var xz=GenerateLinearBestFit(points, out  a, out b);
            var xz2 = GenerateLinearBestFit(points2, out a, out b);
            for ( j = 0; j < xz.Count - 1; j++)
            {
                Line l = new Line();
                Graph.Children.Add(l);
                l.X1 = xz[j].X; l.X2 = xz[j+1].X;
                l.Y1 = xz[j].Y; l.Y2 = xz[j + 1].Y;
                l.Fill= new SolidColorBrush(Color.FromArgb(255, 127, 0, 255));
                l.Stroke= new SolidColorBrush(Color.FromArgb(255, 127, 0, 255));
                l.StrokeThickness = 2;
               
                Thickness thickness = new Thickness(0, 0, 0, 0);
                l.Margin = thickness;

            }
            txtNewSlope.Text = a.ToString("F8");
            txtNewOffset.Text = b.ToString("F8");
        }
        private void btnSend_Click(object sender, RoutedEventArgs e)
        {
            if (IsConnected)
            {
                if (ComPort != null)
                {
                    ComPort.Write(SendedInfo.Text);
                }
            }
                    
        }

        private void cbChnNum_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
           

            if (IsConnected)
            {
                if (ComPort != null)
                {
                    ComPort.Write("c" + cbChnNum.SelectedItem);// set active channel
                    Graph.Children.Clear();
                }
            }
        }

        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
          
            if (IsConnected)
            {
                if (ComPort != null)
                {
                    
                    ComPort.Write("m" + txtNewSlope.Text);
                    ComPort.Write("o" + txtNewOffset.Text);
                    
                }
            }
        }

        private void btnUpdateWaveLegth_Click(object sender, RoutedEventArgs e)
        {
            if (pm != null)
            {
                double d;
                double.TryParse(txtWaveLength.Text, out d);
                // 0 - good.
             int err=   pm.setWavelength(d);
                if (err == 0)
                {
                    MessageBox.Show("Set wave length OK");
                }
            }
        }
        public class XYPoint
        {
            public XYPoint(double x, double y)
            {
                X = x; Y = y;
            }
            public XYPoint()
            {
            }
            public double X;
            public double Y;
        }
        public static List<XYPoint> GenerateLinearBestFit(List<XYPoint> points, out double slope, out double intercept)
        {
            int numPoints = points.Count;
            double meanX = points.Average(point => point.X);
            double meanY = points.Average(point => point.Y);

            double sumXSquared = points.Sum(point => point.X * point.X);
            double sumXY = points.Sum(point => point.X * point.Y);

            slope = (sumXY / numPoints - meanX * meanY) / (sumXSquared / numPoints - meanX * meanX);
            intercept = (slope * meanX - meanY);

            double a1 = slope;
            double b1 = intercept;

            return points.Select(point => new XYPoint() { X = point.X, Y = a1 * point.X - b1 }).ToList();
        }

      
    }
}
