﻿namespace ThorMon
{
    public class PixelCell
    {
        private bool _isSet = false;

        public float Top { get; private set; }
        public float Left { get; private set; }
        public float Width { get; private set; }
        public float Height { get; private set; }
        public bool IsSet { get { return _isSet; } }

        public PixelCell(float left, float top, float width, float height)
        {
            Left = left;
            Top = top;
            Width = width;
            Height = height;
        }

        public void ToggleCell()
        {
            _isSet = !_isSet;
        }

        public void Resize(float width, float height)
        {
            Width = width;
            Height = height;
        }

        public void Reposition(float left, float top)
        {
            Left = left;
            Top = top;
        }

       
    }
}