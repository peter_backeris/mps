//mps_config.h



//define either MPS_96 for 96-well system, or MPS_24 for 24-well system.
//If both are defined, compile error will throw.


//#define MPS_24
#define MPS_96


#define MAJ_VERSION 10
#define MIN_VERSION 0
#define REV_VERSION 0


#define DEFAULT_TIME_UNITS TIME_UNITS_SEC
#define DEFAULT_IRRAD_MODE PERCENT_MAX

static const uint8_t MAX_DC = 63; // limit max dot correction
static const float correction_factor = 1; //global irradiance correction factor for quick calibration adjustment in case of high precision/low accuracy calibration issue
static const uint8_t vled_scalefactor = 69; //conversion factor for led voltage - user set by measuring with voltmeter


//define max channels per group and max number of groups
//If dynamic memory is less than 88%, reduce one of the values to obtain more dynamic memory
#ifdef MPS_24
#include "mps_24.h"
#define MAX_WELLS_PER_GROUP 24
#define MAX_GROUPS 24

#elif defined MPS_96
#include "mps_96.h"
#define MAX_WELLS_PER_GROUP 24
#define MAX_GROUPS 24
#endif

#define NUM_CHANNELS 96


typedef enum{
  TIME_UNITS_MS = 0x00,
  TIME_UNITS_SEC,
  TIME_UNITS_MIN,
  TIME_UNITS_HOUR
}time_units_t;

typedef enum{
  NORMAL = 0x00,
  PERCENT_MAX
}irradiance_mode_t;

typedef enum{
  GROUP_EMPTY = 0x00,
  GROUP_NOT_INITIALIZED,
  GROUP_INITIALIZED,
  GROUP_WAITING_START_DELAY,
  GROUP_ACTIVE,
  GROUP_NOT_ACTIVE,
  GROUP_COMPLETE
}group_state_t;



#if defined MPS_24 && defined MPS_96
#error "MPS_24 MPS_96 defined - can only define one"
#endif
