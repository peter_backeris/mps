/*
 * mps_teensy firmware
 * V 10.0.0
 * 12/14/2021
 * (C) Peter Backeris
 */

#include "mps_config.h"


#include "src/TLC59401/Tlc59401.h"
#include "EEPROM.h"
#include <avr/pgmspace.h>
#include "src/elapsedMillis/elapsedMillis.h"

using namespace std;

/*
*DEFINES
*/

#define UPDATE_GS_FLAG  0x01

//RUN STATE
typedef enum{
  RUN_STATE_IDLE,
  RUN_STATE_GROUPS_INITIALIZED,
  RUN_STATE_EXPERIMENT_RUNNING
}run_state_t;


/*
 * MACROS
 */
#define SERIALprint(x) {Serial.print(x); Serial1.print(x);}
#define SERIALprintln(x) {Serial.println(x); Serial1.println(x);}
#define MAP_CHANNEL(x) {channelMap[x-1]}

static int setWellIrradNormal(uint8_t well_num, uint16_t val);
static int setWellIrradPercentPower(uint8_t well_num, uint16_t val);

/*
 * GLOBALS
 */
static uint8_t g_num_groups;
static uint8_t g_updateFlag; /**< experiment run-time flags */

static run_state_t g_run_state = RUN_STATE_IDLE;

static bool g_endExpFlag = true;
static int8_t g_group_state;

static elapsedMillis experiment_time;

static time_units_t g_time_units = DEFAULT_TIME_UNITS;
static irradiance_mode_t g_irrad_mode = DEFAULT_IRRAD_MODE;

static const uint32_t tmultiplier[] = {1, 1000, 60000, 3600000}; //ms = 0, s = 1, min = 2, hr = 3
static int (*setWellIrrad_fun[])(uint8_t chan, uint16_t irrad) = {setWellIrradNormal, setWellIrradPercentPower};  

/*
 * FUNCTIONSnum_groups
 */

/**
 * measure and return LED voltage
 */
float getLEDVolt(uint8_t scalefactor) {
  float ledvolt = 0;
  for (int i = 0; i < 10; i++) {
    ledvolt += analogRead(1);
  }
  ledvolt = ledvolt / ((float)scalefactor * 10);
  return ledvolt;
}

#define LED_TEENSY 11
#define LED_STATUS 10

/**
 * control blinking of status LEDs on control board
 */
struct status_led_t {
  uint8_t LED = LED_TEENSY;
  uint16_t duration = 0;
  uint16_t off_period = 0;
  bool state = false;
  elapsedMillis blink_timer;

  /**
   * main subroutine for checking and changing LED state
   */
  void blink() {
    if (state) {
      if (blink_timer >= duration) {
        digitalWrite(LED, LOW);
        state = false;
        blink_timer = 0;
      }
    } else if (blink_timer > off_period) {
      digitalWrite(LED, HIGH);
      state = true;
      blink_timer = 0;
    }
  }

  /**
   * manual control of LED state
   */
  void toggle() {
    if (state) {
      digitalWrite(LED, LOW);
      state = false;
    } else {
      digitalWrite(LED, HIGH);
      state = true;
    }
  }

  /**
   * turn LED on. If hold==true, set off period to 0 to maintain on state
   */
  void on (bool hold = true) {
    digitalWrite(LED, HIGH);
    state = true;
    if (hold) off_period = 0;
  }

  /**
   * turn LED off, if hold == true, set off period to 0 to maintain off state
   */
  void off(bool hold = true) {
    digitalWrite(LED, LOW);
    state = false;
    if (hold) (duration = 0);
  }

  /**
   * initialization routine to set LED pin to output
   */
  void init() {
    pinMode(LED_STATUS, OUTPUT);
    pinMode(LED_TEENSY, OUTPUT);
  }

  void running() { //for when experiment is running
    off(false); //teensy_led disabled
    LED = LED_STATUS;

    duration = 100;
    off_period = 100;

  }

  void waiting() { // for when experiment is stopped/cancelled and/or waiting for command
    off(false); //status_led disabled
    LED = LED_TEENSY;
    duration = 50;
    off_period = 950;
  }
    void ready() { // for when groups have been initialized and experiment is ready to start
    off(false); //teensy_led disabled
    LED = LED_STATUS;
    duration = 450;
    off_period = 50;
  }

  void finished() { // after an experiment completes successfully
    duration = 1000;
    off_period = 500;
  }

  void error() { // error occurred, incorrect input, etc
    off(false);
    LED = LED_STATUS;
    duration = 200;
    off_period = 50;
  }

};


//channel and well control functions

  //maps channel to board channel and sets DC and GS values from EEPROM-stored calibration data
void setChannelIrrad(uint8_t chan, uint16_t irrad) {
  uint8_t DCval;
  uint16_t GSval;

  if (irrad == 0) {
    GSval = 0;
  } 
  else {
    int atemp;
    int btemp;
    int ctemp;
    float Aval;
    float Bval;
    float Cval;
    float irradiance = (float)irrad * correction_factor / 100;

    EEPROM.get(sizeof(int) * (chan - 1), atemp);
    EEPROM.get(sizeof(int) * (95 + chan), btemp);
    EEPROM.get(sizeof(int) * (191 + chan), ctemp);
//    Serial.print("Aval: ");
//    Serial.println(atemp);
//    Serial.print("Bval: ");
//    Serial.println(btemp);
//    Serial.print("Cval: ");
//    Serial.println(ctemp);
    Aval = (float)atemp / 10000000;
    Bval = (float)btemp / 100000;
    Cval = (float)ctemp / 10000;
    float DC_f = (-Bval + sqrt((Bval * Bval) - (4 * Aval) * (Cval - irradiance))) / (2 * Aval);

  // check if current is set beyond set limit, set channel to maximum if true
    if (DC_f > MAX_DC) {
      SERIALprint(F("WARNING: Channel "));
      SERIALprint(chan);
      SERIALprintln(F(" is set beyond maximum current limit. Setting channel to max power."));
      DCval = MAX_DC;
      GSval = 4095;
    } else {
      // current is in range, round current up to integer and set PWM to compensate for over-shoot
      DCval = ceil(DC_f);
      float remainder = Aval * pow((float)DCval, 2) + Bval * float(DCval) + Cval - irradiance;
      GSval = round((1 - (remainder / irradiance)) * 4095);
    }
    Tlc.setDC(MAP_CHANNEL(chan), TO_MEMORY, DCval);
    Tlc.setGS(MAP_CHANNEL(chan), TO_MEMORY, GSval);
  }
}

//sets channel gs value to value stored in memory
void restoreGS(uint8_t chan)
{
  Tlc.setGS(MAP_CHANNEL(chan), FROM_MEMORY, 0);
}


void setGS(uint8_t chan, uint16_t val, set_mode setmode)
{
  Tlc.setGS(MAP_CHANNEL(chan), setmode, val);
}


void restoreDC(uint8_t chan)
{
  Tlc.setDC(MAP_CHANNEL(chan), FROM_MEMORY, 0);
}

void setDC(uint8_t chan, uint8_t val, set_mode setmode)
{
  if (val <= MAX_DC){
    Tlc.setDC(MAP_CHANNEL(chan), setmode, val);
  }
  else {
    SERIALprintln(F("DC Value beyond maximum limit - value not changed"));
  }
}

uint8_t getDC(uint8_t chan, bool direct)
{
  return Tlc.getDC(MAP_CHANNEL(chan), direct);
}

uint16_t getGS(uint8_t chan, bool direct)
{
  return Tlc.getGS(MAP_CHANNEL(chan), direct);
}

// calls setIrradiance function based on global irrad_mode
void setWellIrrad(uint8_t well_num, uint16_t val)
{
  setWellIrrad_fun[g_irrad_mode](well_num, val); 
}

static int setWellIrradNormal(uint8_t well_num, uint16_t val)
{
  val = val/NUM_CHANNELS_PER_WELL;
  uint8_t chan1 = ((well_num - 1) * NUM_CHANNELS_PER_WELL) + 1;
  for(uint8_t i = 0; i<NUM_CHANNELS_PER_WELL; i++)
  {
      setChannelIrrad(chan1+i, val);
  }
  return 1;
}

static int setWellIrradPercentPower(uint8_t well_num, uint16_t val)
{
  if (val > 100)
  {
    SERIALprintln(F("Irradiance must be between 0 and 100 percent!"))
    return -1;
  }

  uint8_t value;
  uint8_t base;
  int8_t rem;
  value = (NUM_CHANNELS_PER_WELL * MAX_DC * val)/100;
  base  = value/NUM_CHANNELS_PER_WELL;
  rem   = value%NUM_CHANNELS_PER_WELL;

  uint8_t chan1 = ((well_num - 1) * NUM_CHANNELS_PER_WELL) + 1;
  for (uint8_t i = 0; i < NUM_CHANNELS_PER_WELL; i++)
  {
    value = base + (rem-- > 0);
    SERIALprint(F("Setting Channel "));
    SERIALprint(chan1+i)
    SERIALprint(F(" DC to "));
    SERIALprintln(value);
      
    Tlc.setDC(MAP_CHANNEL(chan1+i), TO_MEMORY, value);
    Tlc.setGS(MAP_CHANNEL(chan1+i), TO_MEMORY, 4095);
  }
  return value;
}


void setWellGS(uint8_t well_num, uint16_t val, set_mode setmode)
{
  uint8_t chan1 = ((well_num - 1) * NUM_CHANNELS_PER_WELL) + 1;

  for(uint8_t i = 0; i<NUM_CHANNELS_PER_WELL; i++)
  {
    setGS(chan1+i, val, setmode);
  }
}

void setWellDC(uint8_t well_num, uint8_t val, set_mode setmode)
{
  uint8_t chan1 = ((well_num - 1) * NUM_CHANNELS_PER_WELL) + 1;
  for(uint8_t i = 0; i<NUM_CHANNELS_PER_WELL; i++)
  {
      setDC(chan1+i, val, setmode);
  }
}

void restoreWellGS(uint8_t well_num)
{
  uint8_t chan1 = ((well_num - 1) * NUM_CHANNELS_PER_WELL) + 1;
  for(uint8_t i = 0; i<NUM_CHANNELS_PER_WELL; i++)
  {
      restoreGS(chan1+i);
  }
}

void restoreWellDC(uint8_t well_num)
{
  uint8_t chan1 = ((well_num - 1) * NUM_CHANNELS_PER_WELL) + 1;
  for(uint8_t i = 0; i<NUM_CHANNELS_PER_WELL; i++)
  {
      restoreDC(chan1+i);
  }
}

void blankWellGS(uint8_t well_num)
{
  uint8_t chan1 = ((well_num - 1) * NUM_CHANNELS_PER_WELL) + 1;
  for(uint8_t i = 0; i<NUM_CHANNELS_PER_WELL; i++)
  {
      setGS(chan1+i, 0, DIRECT);
  }
}

void blankWellDC(uint8_t well_num)
{
  uint8_t chan1 = ((well_num - 1) * NUM_CHANNELS_PER_WELL)+ 1;
  for(uint8_t i = 0; i<NUM_CHANNELS_PER_WELL; i++)
  {
      setDC(chan1+i, 0, DIRECT);
  }
}

struct group_param_t
{
  group_state_t state: 3;
  time_units_t startDelay_timeUnits: 2;
  time_units_t dur_offtime_timeUnits: 2;

};


/**
 * struct to hold group parameters input from user over Serial port
 */
struct group_t {
  group_param_t params;
  uint16_t irrad;
  uint8_t wells[MAX_WELLS_PER_GROUP];
  uint16_t cycles;
  uint16_t remaining_cycles;
  uint16_t duration;
  uint16_t off_period;
  uint16_t start_delay;
  elapsedMillis event_timer;

  /**
   * initialization routine for preparing groups for experiment
   */
  bool init() {
    if (params.state != GROUP_EMPTY) 
    {
      remaining_cycles = cycles;
      uint8_t i = 0;
      while(wells[i]!= 0)
      {
        setWellIrrad(wells[i], irrad);
        i++;
      }
      params.state = GROUP_INITIALIZED; 
      return true;
    } 

    else 
    {
      SERIALprintln(F("GROUP PARAMETERS NOT SET OR SET IMPROPERLY - INITIALIZATION FAILED"));
      return false;
    }
  }

  /**
   * run time update routine to check, set, and return group state
   */
  int8_t update() {
    uint8_t i = 0;
    switch (params.state) {
      case GROUP_INITIALIZED:
        /* initilization state - checks if starting immediately or waiting */
        
        if (start_delay == 0)
        {
          while(wells[i] != 0  & i<MAX_WELLS_PER_GROUP)
          {
            restoreWellGS(wells[i]);
            i++;
          }
        
          params.state = GROUP_ACTIVE;
          g_updateFlag |= UPDATE_GS_FLAG; // sets update flag to update GS and DC registers
          remaining_cycles--;
        }
        else {params.state = GROUP_WAITING_START_DELAY;}
        event_timer = 0;
        break;

      case GROUP_WAITING_START_DELAY:
        /* group is waiting for start_delay
           if num_channels is positive, time values are in seconds
           and must be converted to msec by muliplying by 1000; */

        if (event_timer >= (uint32_t)start_delay * tmultiplier[params.startDelay_timeUnits]) 
        {
          // if start delay is reached, turn on group channels and change group state to 2
          while(wells[i] != 0  & i<MAX_WELLS_PER_GROUP)
          {
            
            restoreWellGS(wells[i]);
            i++;
          }
          g_updateFlag |= UPDATE_GS_FLAG; //sets update flag to update GS and DC registers
          params.state = GROUP_ACTIVE;
          remaining_cycles--;
          event_timer = 0;
        }
        break;

      /* group is on and waiting to turn off */
      case GROUP_ACTIVE:
      // if time values are in seconds, multiply time values by 1000 to get msec
        if (event_timer >= (uint32_t)duration * tmultiplier[g_time_units]) 
        {
          while(wells[i] != 0 & i<MAX_WELLS_PER_GROUP)
          {
            blankWellGS(wells[i]);
            i++;
          }

          if (remaining_cycles == 0) {
            params.state = GROUP_COMPLETE;
          } 
          else
          {
            params.state = GROUP_NOT_ACTIVE;
            event_timer = 0;
          }
            g_updateFlag |= UPDATE_GS_FLAG; //enables GS flag but leaves DC flag set if also required
        }
        break;

      case GROUP_NOT_ACTIVE:
        /* group is turning back on if has remaining cycles */
        if (event_timer >= (uint32_t)off_period * tmultiplier[g_time_units])
        {
          while(wells[i] != 0 & i<MAX_WELLS_PER_GROUP)
          {
            restoreWellGS(wells[i]);
            i++;
          }
          g_updateFlag |= UPDATE_GS_FLAG;
          remaining_cycles--;
          params.state = GROUP_ACTIVE;
          event_timer = 0;
        }
        break;
        
    }
    return params.state;
  }
  /**
   * print group parameters
   */
  void printStatus() {
  // //  SERIALprint(F("Number of channels: "));
  //   SERIALprintln(num_channels);
  //   for (int i = 0; i < abs(num_channels); i++) {
  //     SERIALprint(wells[i].well);
  //     SERIALprint(' ');
  //   }
    SERIALprintln("Wells in group: ");
    for (int i = 0; ((i < MAX_WELLS_PER_GROUP) && (wells[i]>0)); i++) {
       SERIALprint(wells[i]);
       SERIALprint(' ');
    }
   
    SERIALprintln();
    SERIALprint(F("state: "));
    SERIALprintln(params.state);
//    SERIALprint(F("event timer: "));
//    SERIALprintln(event_timer);
    SERIALprint(F("irradiance: "));
    SERIALprintln(irrad);
    SERIALprint(F("duration: "));
    SERIALprintln(duration);
    SERIALprint(F("Start delay: "));
    SERIALprintln(start_delay);
    SERIALprint(F("cycles: "));
    SERIALprintln(cycles);
    SERIALprint(F("off period: "));
    SERIALprintln(off_period);
    SERIALprintln();
  }
};


void updateGS(void)
{
  while(!Tlc.updateGS());
}
void updateDC(void)
{
  Tlc.updateDC();
}

/*
 * global struct instances
 */
group_t groups[MAX_GROUPS] ;

status_led_t status_led;
uint8_t active_channel;
uint8_t active_well;

void setup() {
  analogReference(INTERNAL2V56);
  memset(groups, 0x00, sizeof(groups));
  /* Call Tlc.init() to setup the tlc.
     You can optionally pass an initial PWM value (0 - 4095) for all channels.*/
  Tlc.init();
  Tlc.clearGS();
  Tlc.clearDC();
  updateDC();
  updateGS();

  delay(500);
  Serial.begin(57600);
  Serial1.begin(115200);
  delay(1000);

  SERIALprint(F("mps_teensy Control - " NUM_WELLS_STRING "- well - version "));
  SERIALprint(MAJ_VERSION); SERIALprint(F("."));
  SERIALprint(MIN_VERSION); SERIALprint(F("."));
  SERIALprintln(REV_VERSION);
  SERIALprint(F("Max Groups "));
  SERIALprintln(MAX_GROUPS);
  SERIALprint(F("Max Channels per group: "));
  SERIALprintln(MAX_WELLS_PER_GROUP);
  pinMode(LED_TEENSY, OUTPUT);
  pinMode(LED_STATUS, OUTPUT);
  status_led.waiting();
}

void loop() {

  // check status led state and change if blink period reached
  status_led.blink();

  // read data from either USB or ESP8266 serial port connections
  if (Serial.available()) {
    status_led.on(false);
    serialEvent(&Serial);
    status_led.off(false);
  }
  if (Serial1.available()) {
    status_led.on(false);
    serialEvent(&Serial1);
    status_led.off(false);
  }

  if (g_run_state == RUN_STATE_EXPERIMENT_RUNNING) {

    g_endExpFlag = true;
    g_updateFlag = 0;
    for (int i = 0; i < g_num_groups; i++) {
      g_group_state = groups[i].update();
      // will be true if all groups are on last cycle
      g_endExpFlag = g_endExpFlag & (g_group_state == GROUP_COMPLETE);
    }

    if (g_updateFlag & UPDATE_GS_FLAG) {

      updateGS();
      //TODO Indicate group states here
      SERIALprint(F("LED Voltage: "));
      SERIALprintln(getLEDVolt(vled_scalefactor));
      // updateDC();
      // SERIALprintln("Updated GS registers");
      // print VLED voltage if activating a group for first time
    }

    // if g_endExpFlag is true, all groups are complete (in state 4)
    if (g_endExpFlag) {
      int seconds = experiment_time / 1000;
      int msec = experiment_time % 1000;
      SERIALprint(F("Experiment complete after "));
      SERIALprint(seconds);
      SERIALprint(F(" seconds and "));
      SERIALprint(msec);
      SERIALprintln(F(" milliseconds."));
      g_run_state = RUN_STATE_IDLE;
      status_led.finished();
    }
  }
}

void Warn_Experiment_Running() {
  SERIALprintln(F("Command cannot be accepted while experiment is initialized or running."));
}

#define CHECK_EXPERIMENT_NOT_RUNNING() \
        if (g_run_state >= RUN_STATE_GROUPS_INITIALIZED) { \
          Warn_Experiment_Running();\
          break;\
        }

// Serial command handling serial protocol
template <typename T> void serialEvent(T *p) {
  int16_t val;
  char cmd;
  while (p->available()) {
    cmd = p->read();

    switch (cmd) {
      //***Following Commands are for Individual Channel Control and testing***
      case 'c':
        //set active channel number
        /* this modifies the char_param_t active_channel chan value
            use 'p' and 'd' to change GS and DC respectively for the active_channel
            use 'u' to refresh DC and GS registers with active_channel parameters
        */
        CHECK_EXPERIMENT_NOT_RUNNING();
        val = p->parseInt();
        active_channel = val;
        restoreDC(val);
        restoreGS(val);
        SERIALprint(F("Setting active LED channel to "));
        SERIALprintln(val);
        break;

      // set active channel PWM val
      case 'o':
        CHECK_EXPERIMENT_NOT_RUNNING();
        val = p->parseInt();
        setGS(active_channel, val, TO_MEMORY_DIRECT);

        SERIALprint(F("Setting channel "));
        SERIALprint(active_channel);
        SERIALprint(F(" grayscale to "));
        SERIALprintln(val);
        updateGS();
        break;

      // set active channel dot-correction value
      case 'd':
        CHECK_EXPERIMENT_NOT_RUNNING();
        val = p->parseInt();
        if (val > MAX_DC) {
          SERIALprintln(F("Value is beyond maximum DC value allowed - setting LEDs to max allowed"));
          val = MAX_DC;
        }
        setDC(active_channel, (uint8_t)val, TO_MEMORY_DIRECT);
        SERIALprint(F("Setting channel "));
        SERIALprint(active_channel);
        SERIALprint(F(" dot-correction to "));
        SERIALprintln(val);
        updateDC();
        break;

     //restore active channel's grayscale value from memory, print before and after values
     case 'z':
     
        SERIALprint("GSdatval before restore: ");
        SERIALprintln(getGS(active_channel, true));
        SERIALprint("GSmemval before restore: ");
        SERIALprintln(getGS(active_channel, false));
        setGS(active_channel, 0, FROM_MEMORY);
        updateGS();
        SERIALprint("GSdatval after restore: ");
        SERIALprintln(getGS(active_channel, true));
        
        break; 
#if 0
            case 'k':// perform ramp output of individual LED by ramping from 0 to 63 dc at 4095 PWM
              val = p->parseInt();//get channel number
              ichannel.chan = val;
              Tlc.clear();
              Tlc.update();
              ichannel.setGS(4095);
              ichannel.setDC(0);
              Tlc.updateDC();
              while (Tlc.update());
              SERIALprint(F("Calibrating channel ")); SERIALprintln(val);
              delay(1500);
              for (int i = 0; i < 64; i += 5 ) {
                SERIALprintln(i);
                ichannel.setDC(i);
                Tlc.updateDC();
                delay(1000);



              }
              delay(100);
              Tlc.clear();
              Tlc.update();
              break;
#endif

      case 't':
      {
        /* test individual LEDs by turning on one at a time
         * uses active_channel DC and GS vals to apply to each LED
         * send x to cancel
         */
        CHECK_EXPERIMENT_NOT_RUNNING();
        val = p->parseInt(); // read in delay time
        uint16_t gsval = getGS(active_channel, false);
        uint8_t dcval = getDC(active_channel, false);
        SERIALprint(F("GS VAL: "));
        SERIALprintln(gsval);
        SERIALprint(F("DC VAL: "));
        SERIALprintln(dcval);
        
        for (int i = 1; i <= NUM_CHANNELS; i++) {
          Tlc.clearGS();
          Tlc.clearDC();
          active_channel = i;
          setGS(active_channel, gsval, DIRECT);
          setDC(active_channel, dcval, DIRECT);
          updateGS();
          updateDC();
          SERIALprint(F("Channel "));
          SERIALprintln(i);
          delay(val);
          while (p->available()) {
            if (p->read() == 'x') break;
          }
        }
        Tlc.clearGS();
        updateGS();
      }
        break;
    
      case 'T':
      {
        /* test each well sequentially
         * send x to cancel
         */
        CHECK_EXPERIMENT_NOT_RUNNING();
        val = p->parseInt(); // read in delay time
//        uint16_t gsval = getGS(active_channel, false);
//        uint8_t dcval = getDC(active_channel, false);
        Tlc.clearGS();
        Tlc.clearDC();
        for (int i = 1; i <= NUM_WELLS; i++) {
//          setWellGS(i,gsval,DIRECT);
//          setWellDC(i,dcval,DIRECT);
          restoreWellGS(i);
          restoreWellDC(i);
          updateDC();
          updateGS();
          SERIALprint(F("Well "));
          SERIALprintln(i);
          delay(val);
          while (p->available()) {
            if (p->read() == 'x') break;
          }
          Tlc.clearDC(); 
          Tlc.clearGS();
          updateDC();
          updateGS();
        }
        
      }
        break;

    
      // write current GS and DC values to drivers
      case 'u':
        CHECK_EXPERIMENT_NOT_RUNNING();
        SERIALprintln(F("Updating DC and GS registers"));
        // SERIALprint(F("Channel: "));
        // SERIALprintln(active_channel.chan);
        // SERIALprint(F("GS: "));
        // SERIALprintln(active_channel.GSval);
        // SERIALprint(F("DC: "));
        // SERIALprintln(active_channel.DCval);
        updateDC();
        updateGS();
      
        break;
      //restore all GS and DC values from memory, does not write to drivers (must send 'u' after)
      case 'U':
        CHECK_EXPERIMENT_NOT_RUNNING();
        SERIALprintln(F("Restoring DC and GS registers from memory"));
        Tlc.restoreAllGS();
        Tlc.restoreAllDC();
        break;
/***** CONTROLS FOR ALL LEDS SIMULTANEOUSLY **********/
      // set all channels to the specified GS value
      case 'a':
        CHECK_EXPERIMENT_NOT_RUNNING();
        val = p->parseInt();
        Tlc.setAllGS(val, true);
        updateGS();
        SERIALprint(F("All Channels GS set to: "));
        SERIALprintln(val);
        break;

      // set all channels to specified DC (current) value
      case 'b':
        CHECK_EXPERIMENT_NOT_RUNNING();
        val = p->parseInt();
        if (val > MAX_DC) {
          SERIALprintln(F("Value is beyond maximum DC value allowed - setting LEDs to max allowed"));
          val = MAX_DC;
        }
        Tlc.setAllDC(val,true);
        updateDC();
        SERIALprint(F("All channels DC set to: "));
        SERIALprintln(val);
        break;

      // set active channel to specified irradiance value (requires calibration)
      case 'e':
        CHECK_EXPERIMENT_NOT_RUNNING();
        val = p->parseInt();
      
        SERIALprint(F("Setting channel "));
        SERIALprint(active_channel);
        SERIALprint(F(" irradiance to "));
        SERIALprint((float)val / 100);
        SERIALprintln(F(" mW/cm2"));
        setChannelIrrad(active_channel, val);
        restoreDC(active_channel);
        restoreGS(active_channel);
        updateDC();
        updateGS();

        break;

      // set all channels to specified irradiance value (requires calibration)
      case 'E':
        CHECK_EXPERIMENT_NOT_RUNNING();
        val = p->parseInt();
        SERIALprint(F("Setting all channels irradiance to "));
        SERIALprint((float)val / 100);
        SERIALprintln(F(" mW/cm2"));

        for (uint8_t i = 1; i<=NUM_CHANNELS; i++)
        {
          setChannelIrrad(i, val);
          restoreGS(i);
          restoreDC(i);
        }

        updateDC();
        updateGS();   
        break;

    /**** CONTROLS FOR SELECTED AND ALL WELLS *****/    
      //set active well number TODO: check bounds
      case 'w':
        CHECK_EXPERIMENT_NOT_RUNNING();
        val = p->parseInt();
        active_well = val;
        SERIALprint(F("Setting active well to "));
        SERIALprintln(val);
        break;

      // set all wells to specified irradiance value (mW/cm2 *100);
      case 'R':
        CHECK_EXPERIMENT_NOT_RUNNING();
        val = p->parseInt();
        Tlc.clearGS();
        updateGS();

        SERIALprint(F("Setting all wells to irradiance of "));
        SERIALprint(float(val) / 100);
        SERIALprintln(F(" mW/cm2"));
        for (int i = 1; i <= NUM_WELLS; i++)
        {
          setWellIrrad(i, val);
          restoreWellGS(i);
          restoreWellDC(i);
        }
        updateDC();
        updateGS();       
  
        break;
       // set active well to specified irradiance value (mW/cm2 *100);
      case 'r':
        CHECK_EXPERIMENT_NOT_RUNNING();
        val = p->parseInt();  
        SERIALprint(F("Setting well "));
        SERIALprint(active_well);
        SERIALprint(F(" irradiance to "));
        SERIALprint((float)val / 100);
        SERIALprintln(F(" mW/cm2"));
        setWellIrrad(active_well, val);
        restoreWellGS(active_well);
        restoreWellDC(active_well); 
        updateDC();
        updateGS();
        break;
       case 'p':
        CHECK_EXPERIMENT_NOT_RUNNING();
        val = p->parseInt();
        SERIALprint(F("Setting active well to power of "));
        SERIALprint(val);
        SERIALprintln(F(" percent"));
        if(setWellIrradPercentPower(active_well, val)<0)
        {
          Tlc.restoreAllDC();
          Tlc.restoreAllGS();
          updateGS();
          updateDC();
        }
        break;
       case 'P':
        CHECK_EXPERIMENT_NOT_RUNNING();
        val = p->parseInt();
        SERIALprint(F("Setting all wells to power of "));
        SERIALprint(val);
        SERIALprintln(F(" percent"));
        for (int i = 1; i<=NUM_WELLS; i++)
        {
            if(setWellIrradPercentPower(i, val)<0)
            break;
        }
       
        Tlc.restoreAllDC();
        Tlc.restoreAllGS();
        updateGS();
        updateDC();


        break;


      case 'y':
        CHECK_EXPERIMENT_NOT_RUNNING();
        val = p->parseInt();
        SERIALprint(F("Setting well "));
        SERIALprint(active_well);
        SERIALprint(F(" GS to "));
        SERIALprintln(val);

        setWellGS(active_well, val, TO_MEMORY_DIRECT);
        updateGS();
        break;

      case 'h':
        CHECK_EXPERIMENT_NOT_RUNNING();
        val = p->parseInt();
        SERIALprint(F("Setting well "));
        SERIALprint(active_well);
        SERIALprint(F(" DC to "));
        SERIALprintln(val);

        setWellDC(active_well, val, TO_MEMORY_DIRECT);
        updateDC();
        break;
    
      //***The following commands control experimental parameters and execution

      // define number of groups to be utilized
      case 'n':
        CHECK_EXPERIMENT_NOT_RUNNING();
        val = p->parseInt();
        g_num_groups = val;
        SERIALprint(F("Number of groups set to "));
        SERIALprintln(val);
        break;


      // enter individual set of group parameters
      case 'g':
      {
        /*  
           the following syntax is used to enter group parameters g[group_number],[num_channels],[channel i],....[last channel], [irradiance], [duration],[start_delay], [off_period], [number of cycles]

           TODO: Replace this method with byte-protocol to receive entire experiment in 1 command
        */
        CHECK_EXPERIMENT_NOT_RUNNING();
        val = p->parseInt() - 1;
        if (val > (g_num_groups - 1) || (g_num_groups < 1) || (val < 0)) {
          SERIALprint(F("Invalid Group Number. Check group number is greater than 0 and less than "));
          SERIALprint(g_num_groups);
          SERIALprintln(F("(number of groups specified)"));
          status_led.error();
          break;
        }
        groups[val].params.startDelay_timeUnits = TIME_UNITS_MS;// TODO: what is this supposed to mean?: set this to 0 before all settings are complete, change to 1 after to indicate settings sets properly

        int8_t nwells = p->parseInt();
        if(abs(nwells) > MAX_WELLS_PER_GROUP)
        {
          SERIALprint(F("Number of wells exceeds maximum allowed per group: "));
          SERIALprintln(MAX_WELLS_PER_GROUP);
          status_led.error();
          break;
        }
        int j;
        for (j=0; j < abs(nwells); j++) {
          groups[val].wells[j] = p->parseInt();
        }
        //ensure that the following well in the group is set to 0 to indicate end of wells in group
        if(j+1 < MAX_WELLS_PER_GROUP)
        {
          groups[val].wells[j+1] = 0;
        }
        
        groups[val].irrad = p->parseInt();
        groups[val].duration = p->parseInt();
        groups[val].start_delay = p->parseInt();
        groups[val].off_period = p->parseInt();
        groups[val].cycles = p->parseInt();
        groups[val].params.state = GROUP_NOT_INITIALIZED;
        groups[val].params.startDelay_timeUnits = TIME_UNITS_SEC; 
        // if (nchannels<1) //units are in ms for duration, off-time
        // {
        //   groups[val].params.dnunits = 0;
        //   groups[val].params.otunits =0;
        // }
        // else
        // {
        //   groups[val].params.dnunits = 1;
        //   groups[val].params.otunits = 1;

        // }

        SERIALprint(F("Group "));
        SERIALprint(val + 1);
        SERIALprintln(F(" received"));
      }
        break;

      // initialize groups from group_params data. Group params must first be initialized (see 'g' command).
      // Individual channel dc and gs values are calculated from calibration data
      case 'i':
        CHECK_EXPERIMENT_NOT_RUNNING();
        Tlc.clearGS();
        updateGS();
        SERIALprint(F("Initializing "));
        SERIALprint(g_num_groups);
        SERIALprintln(" groups:");

        if (g_num_groups > 0) {
          for (uint8_t i = 0; i < g_num_groups; i++) {
            SERIALprint(F("Group "));
            SERIALprintln(i + 1);
            if (groups[i].init()) {
              groups[i].printStatus();
              delay(100);
            } 
            else
            {
              SERIALprint(F("Group "));
              SERIALprint(i + 1);
              SERIALprintln(F(" parameters not set. Groups not initialized."));
              SERIALprintln(F("Ensure group parameters are set and try initializing again"));
              status_led.error();
              return;
            }
          }
          Tlc.restoreAllDC();
          updateDC();
          g_run_state = RUN_STATE_GROUPS_INITIALIZED;
          status_led.ready();
        } else {
          SERIALprintln(F("No group parameters to load"));
          status_led.error();
        }
        break;



      //start experiment - 'i' must first be sent to initialize groups
      case 's':
//        CHECK_EXPERIMENT_NOT_RUNNING();
        if (g_run_state == RUN_STATE_GROUPS_INITIALIZED) { // check that groups have been initialized
          g_run_state = RUN_STATE_EXPERIMENT_RUNNING;
          SERIALprintln(F("Experiment Started"));
          status_led.off(true);
          status_led.running();
          experiment_time = 0;
          return;
        } else if (g_run_state == RUN_STATE_IDLE) {
          SERIALprintln(F("Groups not initialized"));
          status_led.error();
        }
        break;


      // stop experiment and/or turn off all LEDs
      case 'x':
        Tlc.clearGS();
        Tlc.clearDC();
        updateDC();
        updateGS();
        if (g_run_state == RUN_STATE_EXPERIMENT_RUNNING) {
          g_run_state  = RUN_STATE_IDLE;
          int seconds = experiment_time / 1000;
          SERIALprint(F("Experiment stopped after "));
          SERIALprint(seconds);
          SERIALprintln(F(" seconds"));
          SERIALprintln(F("Re-initialze groups before starting again"));
                       
        } else {
          g_run_state  = RUN_STATE_IDLE;
          SERIALprintln(F("LEDs cleared but no experiment was running"));
        }
        status_led.waiting();
        break;

      //reset all channel settings back to 0 
      case 'X':
        CHECK_EXPERIMENT_NOT_RUNNING();
        Tlc.clearGSmem();
        Tlc.clearGS();
        Tlc.clearDCmem();
        Tlc.clearDC();
        updateDC();
        updateGS();
        memset(groups, 0x00, sizeof(groups));
        SERIALprintln(F("Group and channel memory cleared"));                                  
        break;

      //set irradiance mode 0 = normal, 1 = percent max power
      case 'j':
        CHECK_EXPERIMENT_NOT_RUNNING();
        val = p->parseInt();
        if (val == 0)
        {
          g_irrad_mode = NORMAL;
          SERIALprintln(F("Setting Well Irradiance Mode to Normal (using calibration for mW/cm2)"));
        }

        else if (val == 1)
        {
          g_irrad_mode = PERCENT_MAX;
          SERIALprintln(F("Setting Well Irradiance Mode to Percent-Max"));
        }
        else
           SERIALprintln(F("Value must be 1 or 0!!"));
        break;
                                   
      //change time units for group duration and off-time values
      //0 = ms, 1 = s, 2 = m, 3 = h   
      case 'm':
        CHECK_EXPERIMENT_NOT_RUNNING();
        val = p->parseInt();
        if (val == 0)
        {

          g_time_units = TIME_UNITS_SEC;
          SERIALprintln(F("Setting time units to seconds"));
        }
        else if (val == 1)
        {
          g_time_units = TIME_UNITS_MS;
          SERIALprintln(F("Setting time units to milliseconds"));
        }

        else { SERIALprintln(F("Invalid Value: enter m1 for milliseconds or m0 for seconds time units")); }
        break;
  

    

      //***MISCELLANEOUS COMMANDS
      // output LED voltage measured by TEENSY
      case 'v':
        SERIALprint(F("LED Voltage: "));
        SERIALprintln(getLEDVolt(vled_scalefactor));
        break;

      case 'A':
        CHECK_EXPERIMENT_NOT_RUNNING();
        val = p->parseInt();
        EEPROM.put((active_channel - 1)*sizeof(val), val);
        SERIALprint(F("Setting Coefficient A for active channel to "));
        SERIALprintln(val);
        break;

      case 'B':
        CHECK_EXPERIMENT_NOT_RUNNING();
        val = p->parseInt();
        EEPROM.put((95 + active_channel)*sizeof(val), val);
        SERIALprint(F("Setting coefficient B for active channel to "));
        SERIALprintln(val);
        break;

      case 'C':
        CHECK_EXPERIMENT_NOT_RUNNING();
        val = p->parseInt();
        EEPROM.put((191 + active_channel)*sizeof(val), val);
        SERIALprint(F("Setting coefficient C for active channel to "));
        SERIALprintln(val);
        break;

      default:
        // no command - empty buffer
        p->read();
        break;
    }
  }
}
