/*  Copyright (c) 2017 by Peter Backeris
    
    The original work by Alex Leone was modified to support additional features 
    and falls under the same license as described below

*/


/*  Copyright (c) 2009 by Alex Leone <acleone ~AT~ gmail.com>

    This file is part of the Arduino TLC5940 Library.

    The Arduino TLC5940 Library is free software: you can redistribute it
    and/or modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    The Arduino TLC5940 Library is distributed in the hope that it will be
    useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with The Arduino TLC5940 Library.  If not, see
    <http://www.gnu.org/licenses/>. */




//File is intended for use with the TLC59401 LED Driver

#include "Tlc59401.h"
#include "pinouts/pin_functions.h"


/** This will be true (!= 0) if update was just called and the data has not
    been latched in yet. */
volatile uint8_t tlc_needXLAT;

/** Some of the extended library will need to be called after a successful
    update. */
volatile void (*tlc_onUpdateFinished)(void);

/** Packed grayscale data, 24 bytes (16 * 12 bits) per TLC.

    Format: Lets assume we have 2 TLCs, A and B, daisy-chained with the SOUT of
    A going into the SIN of B.
    - byte 0: upper 8 bits of B.15
    - byte 1: lower 4 bits of B.15 and upper 4 bits of B.14
    - byte 2: lower 8 bits of B.0
    - ...
    - byte 24: upper 8 bits of A.15
    - byte 25: lower 4 bits of A.15 and upper 4 bits of A.14
    - ...
    - byte 47: lower 8 bits of A.0

    \note Normally packing data like this is bad practice.  But in this
          situation, shifting the data out is really fast because the format of
          the array is the same as the format of the TLC's serial interface. */


uint8_t tlc_GSData[NUM_TLCS * 24];
uint8_t tlc_DCData[NUM_TLCS * 12];

uint8_t tlc_GSMemory[NUM_TLCS * 24];
uint8_t tlc_DCMemory[NUM_TLCS * 12];



/** Interrupt called after an XLAT pulse to prevent more XLAT pulses. */
static inline void Tlc59401_interrupt(void)
{
    disable_XLAT_pulses();
    clear_XLAT_interrupt();
    tlc_needXLAT = 0;
    if (tlc_onUpdateFinished) {
        sei();
        tlc_onUpdateFinished();
    }
}

#if defined(__AVR__)
ISR(TIMER1_OVF_vect)
{
    Tlc59401_interrupt();
}

#elif defined(__arm__) && defined(TEENSYDUINO)
void ftm1_isr(void)
{
    uint32_t sc = FTM1_SC;
    if (sc & 0x80) FTM1_SC = sc & 0x7F;
    Tlc59401_interrupt();
}

#endif


/** Pin i/o and Timer setup.  The grayscale register will be reset to all
    zeros, or whatever initialValue is set to and the Timers will start.
    \param initialValue = 0, optional parameter specifing the inital startup
           value */
void Tlc59401::init(uint16_t initialValue)
{
    /* Pin Setup */
    output_pin(XLAT_DDR, XLAT_PIN);
    output_pin(BLANK_DDR, BLANK_PIN);
    output_pin(GSCLK_DDR, GSCLK_PIN);
    output_pin(VPRG_DDR, VPRG_PIN);
    clear_pin(VPRG_PORT, VPRG_PIN);  // grayscale mode (VPRG low)
#if XERR_ENABLED
    pullup_pin(XERR_DDR, XERR_PORT, XERR_PIN); // XERR as input, enable pull-up resistor
#endif
    set_pin(BLANK_PORT, BLANK_PIN); // leave blank high (until the timers start)

    tlc_shift8_init();

    clearDC();
    clearDCmem();
    clearGS();
    clearGSmem();

    disable_XLAT_pulses();
    clear_XLAT_interrupt();
    tlc_needXLAT = 0;
    pulse_pin(XLAT_PORT, XLAT_PIN);


    /* Timer Setup */
#if defined(__AVR__)
    /* Timer 1 - BLANK / XLAT */
    TCCR1A = _BV(COM1B1);  // non inverting, output on OC1B, BLANK
    TCCR1B = _BV(WGM13);   // Phase/freq correct PWM, ICR1 top
    OCR1A = 4;             // duty factor on OC1A, XLAT is inside BLANK
    OCR1B = 8;             // duty factor on BLANK (larger than OCR1A (XLAT))
    ICR1 = TLC_PWM_PERIOD; // see tlc_config.h

    /* Timer 2 - GSCLK */
#if defined(TLC_ATMEGA_8_H)
    TCCR2  = _BV(COM20)       // set on BOTTOM, clear on OCR2A (non-inverting),
           | _BV(WGM21);      // output on OC2B, CTC mode with OCR2 top
    OCR2   = TLC_GSCLK_PERIOD / 2; // see tlc_config.h
    TCCR2 |= _BV(CS20);       // no prescale, (start pwm output)
#elif defined(TLC_TIMER3_GSCLK)
    TCCR3A = _BV(COM3A1)      // set on BOTTOM, clear on OCR3A (non-inverting),
                              // output on OC3A
           | _BV(WGM31);      // Fast pwm with ICR3 top
    OCR3A = 0;                // duty factor (as short a pulse as possible)
    ICR3 = TLC_GSCLK_PERIOD;  // see tlc_config.h
    TCCR3B = _BV(CS30)        // no prescale, (start pwm output)
           | _BV(WGM32)       // Fast pwm with ICR3 top
           | _BV(WGM33);      // Fast pwm with ICR3 top
#else
    TCCR2A = _BV(COM2B1)      // set on BOTTOM, clear on OCR2A (non-inverting),
                              // output on OC2B
           | _BV(WGM21)       // Fast pwm with OCR2A top
           | _BV(WGM20);      // Fast pwm with OCR2A top
    TCCR2B = _BV(WGM22);      // Fast pwm with OCR2A top
    OCR2B = 0;                // duty factor (as short a pulse as possible)
    OCR2A = TLC_GSCLK_PERIOD; // see tlc_config.h
    TCCR2B |= _BV(CS20);      // no prescale, (start pwm output)
#endif
    TCCR1B |= _BV(CS10);      // no prescale, (start pwm output)

#elif defined(__arm__) && defined(TEENSYDUINO)
    clear_pin(XLAT_DDR, XLAT_PIN);
    SIM_SCGC4 |= SIM_SCGC4_CMT;
    CMT_MSC = 0;
    CMT_PPS = 0;
    CMT_CGH1 = TLC_TIMER_TEENSY3_NORMAL_CGH1;
    CMT_CGL1 = TLC_TIMER_TEENSY3_NORMAL_CGL1;
    CMT_CMD1 = 1;
    CMT_CMD2 = 0;
    CMT_CMD3 = 0;
    CMT_CMD4 = 0;
    CMT_OC = 0x60;
    CMT_MSC = 0x01;
    CORE_PIN5_CONFIG = PORT_PCR_MUX(2)|PORT_PCR_DSE|PORT_PCR_SRE;
    FTM1_SC = 0;
    FTM1_MOD = TLC_TIMER_TEENSY3_NORMAL_MOD;
    FTM1_CNT = 0;
    FTM1_C0SC = 0x24;
    FTM1_C1SC = 0x24;
    FTM1_C0V = TLC_TIMER_TEENSY3_NORMAL_MOD - TLC_TIMER_TEENSY3_NORMAL_CV;
    FTM1_C1V = TLC_TIMER_TEENSY3_NORMAL_MOD - TLC_TIMER_TEENSY3_NORMAL_CV - 1;
    FTM1_SC = FTM_SC_CLKS(1) | FTM_SC_CPWMS;
    NVIC_ENABLE_IRQ(IRQ_FTM1);
    CORE_PIN4_CONFIG = PORT_PCR_MUX(3)|PORT_PCR_DSE|PORT_PCR_SRE;
#endif
    updateGS();
    updateDC();
}

/** Clears the grayscale data array, #tlc_GSData, but does not shift in any
    data.  This call should be followed by update() if you are turning off
    all the outputs. */
void Tlc59401::clearGS(void)
{
    memset(tlc_GSData, 0x00, sizeof(tlc_GSData));
}

void Tlc59401::clearGSmem(void)
{
    memset(tlc_GSMemory, 0x00, sizeof(tlc_GSMemory));
}



void Tlc59401::restoreAllGS(void)
{
    memcpy(tlc_GSData,tlc_GSMemory,sizeof(tlc_GSMemory));
}



/** Shifts in the data from the grayscale data array, #tlc_GSData.
    If data has already been shifted in this grayscale cycle, another call to
    update() will immediately return 1 without shifting in the new data.  To
    ensure that a call to update() does shift in new data, use
    \code while(Tlc.update()); \endcode
    or
    \code while(tlc_needXLAT); \endcode
    \returns 1 if there is data waiting to be latched, 0 if data was
             successfully shifted in */
uint8_t Tlc59401::updateGS(void)
{
    if (tlc_needXLAT) {
        return 1;
    }
    disable_XLAT_pulses();

    //add more delay after xlat pulse
    for (int i = 0; i<100; i++){
      __asm__("nop\n\t");
    }

    uint8_t *p = tlc_GSData;
    //blank shifts to increase reliability
    tlc_shift8(0);
    tlc_shift8(0);
    while (p < tlc_GSData + NUM_TLCS * 24) {
        tlc_shift8(*p++);
        tlc_shift8(*p++);
        tlc_shift8(*p++);
    }
    //add delay before next xlat pulse
    for (int i = 0; i<100; i++){
      __asm__("nop\n\t");
    }
    tlc_needXLAT = 1;
    enable_XLAT_pulses();
    set_XLAT_interrupt();
    return 0;
}




/** Sets channel to value in the grayscale data array, #tlc_GSData.
    \param channel (0 to #NUM_TLCS * 16 - 1).  OUT0 of the first TLC is
           channel 0, OUT0 of the next TLC is channel 16, etc.
    \param value (0-4095).  The grayscale value, 4095 is maximum.
    \see get */
void Tlc59401::setGS(TLC_CHANNEL_TYPE channel, set_mode setmode, uint16_t value)
{
    TLC_CHANNEL_TYPE index8 = (NUM_TLCS * 16 - 1) - channel;
    uint8_t *index12pMem =   tlc_GSMemory + (((uint16_t)index8 *3) >> 1);
    uint8_t *index12pDat =   tlc_GSData   + (((uint16_t)index8 *3) >> 1);
    uint8_t *index12p;

    switch (setmode)
    {
        case FROM_MEMORY:
            value = (index8 & 1)? // starts in the middle
            (((uint16_t)(*index12pMem & 15)) << 8) | // upper 4 bits
            *(index12pMem + 1)                       // lower 8 bits
            : // starts clean
            (((uint16_t)(*index12pMem)) << 4) | // upper 8 bits
            ((*(index12pMem + 1) & 0xF0) >> 4); // lower 4 bits
        case DIRECT:
            index12p = index12pDat;
            break;
        case TO_MEMORY_DIRECT:
        case TO_MEMORY:
            index12p = index12pMem;
            break; 
        default:
            return;
    }
    if (index8 & 1) { // starts in the middle
                      // first 4 bits intact | 4 top bits of value
        *index12p = (*index12p & 0xF0) | (value >> 8);
              // 8 lower bits of value
        *(++index12p) = value & 0xFF;
    } 
    else { // starts clean
              // 8 upper bits of value
        *(index12p++) = value >> 4;
              // 4 lower bits of value | last 4 bits intact
        *index12p = ((uint8_t)(value << 4)) | (*index12p & 0xF);
    }

    if (setmode == TO_MEMORY_DIRECT)
    {
        setGS(channel, DIRECT, value);
    }
}

 
 /** Gets the current grayscale value for a channel
    \param channel (0 to #NUM_TLCS * 16 - 1).  OUT0 of the first TLC is
           channel 0, OUT0 of the next TLC is channel 16, etc.
    \param direct, false = use value for channel memory value, true = use directly set value
    \returns current grayscale value (0 - 4095) for channel
    \
    \see set */
uint16_t Tlc59401::getGS(TLC_CHANNEL_TYPE channel, bool direct)
{
    TLC_CHANNEL_TYPE index8 = (NUM_TLCS * 16 - 1) - channel;
    uint8_t *index12p;
    if (direct) index12p = tlc_GSData + ((((uint16_t)index8) * 3) >> 1);
    else index12p = tlc_GSMemory + ((((uint16_t)index8)*3) >> 1);

    uint16_t value = (index8 & 1)? // starts in the middle
        (((uint16_t)(*index12p & 0x0F)) << 8) | // upper 4 bits
        *(index12p + 1)                       // lower 8 bits
    : // starts clean
        (((uint16_t)(*index12p)) << 4) | // upper 8 bits
        ((*(index12p + 1) & 0xF0) >> 4); // lower 4 bits

    return value;
}

    // uint16_t value =  (index8 & 1) ? // starts in the middle
    //         ((*(uint16_t*)(index12p))) 
    //                                     // lower 8 bits
    //     : // starts clean
    //         ((*(uint16_t*)(index12p)));// upper 8 bits
    //        // lower 4 bits
    // // that's probably the ugliest ternary operator I've ever created
    //         return value;


/** Sets all channels to value.
    \param value grayscale value (0 - 4095) */
void Tlc59401::setAllGS(uint16_t value, bool memory)
{
    uint8_t firstByte = value >> 4;
    uint8_t secondByte = (value << 4) | (value >> 8);
    uint8_t *p = tlc_GSData;
    while (p < tlc_GSData + NUM_TLCS * 24) {
        *p++ = firstByte;
        *p++ = secondByte;
        *p++ = (uint8_t)value;
    }
    if (memory)
    {
        p = tlc_GSMemory;
        while (p < tlc_GSMemory + NUM_TLCS * 24) {
            *p++ = firstByte;
            *p++ = secondByte;
            *p++ = (uint8_t)value;
         }
    }
}

void Tlc59401::setAllDC(uint8_t value, bool memory)
{

    uint8_t firstByte = value << 2 | value >> 4;
    uint8_t secondByte = value << 4 | value >> 2;
    uint8_t thirdByte = value << 6 | value;
    uint8_t *p = tlc_DCData;
    while(p < tlc_DCData + NUM_TLCS * 12){
      *p++ = firstByte;
      *p++ = secondByte;
      *p++ = thirdByte;
    }

    if(memory)
    {
        p = tlc_DCMemory;
        while(p < tlc_DCMemory + NUM_TLCS * 12){
          *p++ = firstByte;
          *p++ = secondByte;
          *p++ = thirdByte;
        } 
    }


}

void Tlc59401::updateDC(void)
{

  while(tlc_dcModeStart());

  //pulse_pin(XLAT_PORT, XLAT_PIN); //NEW LINE ADDED HERE _ TEST THIS
  uint8_t *p = tlc_DCData;

  //delay(1);
  //pulse_pin(SCLK_PORT, SCLK_PIN);
  tlc_shift8(0);
  while (p < tlc_DCData + NUM_TLCS * 12) {
      tlc_shift8(*p++);
      tlc_shift8(*p++);
      tlc_shift8(*p++);
  }
  tlc_needXLAT = 1;
  enable_XLAT_pulses();
  set_XLAT_interrupt();


  //pulse_pin(XLAT_PORT, XLAT_PIN);
  //pulse_pin(XLAT_PORT, XLAT_PIN);
  while(tlc_dcModeStop());


}



//clears the dot correction data array
void Tlc59401::clearDC(void)
{
    memset(tlc_DCData, 0x00, sizeof(tlc_DCData));
}

void Tlc59401::clearDCmem(void)
{
    memset(tlc_DCMemory, 0x00, sizeof(tlc_DCMemory));
}

void Tlc59401::restoreAllDC(void)
{
    memcpy(tlc_DCData,tlc_DCMemory,sizeof(tlc_DCMemory));
}


void Tlc59401::setDC(TLC_CHANNEL_TYPE channel, set_mode setmode, uint8_t value)
{
    TLC_CHANNEL_TYPE index8 = (NUM_TLCS * 16 -1) - channel;
    uint8_t *index6pMem = tlc_DCMemory + ((((uint16_t)index8)*3)>>2);
    uint8_t *index6pDat = tlc_DCData   + ((((uint16_t)index8)*3)>>2);
    uint8_t *index6p;
     
    switch (setmode)
    {
        case FROM_MEMORY:
        {
            uint8_t position = index8%4;
            switch (position)
            {   
                case 0:
                    value = (*index6pMem & 0xFC ); //assign first 6bits memory to value
                    value = (*index6pDat & 0x03) | value; // assign last 2 bits data to value;
                    *index6pDat = value; // assign value to Data
                    break;
                case 1:
                    value = (*index6pDat & 0xFC);
                    value = value | (*index6pMem & 0x03);
                    *index6pDat = value;
                    index6pDat++;
                    index6pMem++;
                    value = (*index6pMem & 0xF0); // first 4 bits of memory
                    value = value | (*index6pDat & 0x0F);
                    *index6pDat = value;
                    break;
                case 2:
                    value = (*index6pDat & 0xF0); //get first 4 bits of data;
                    value = value | (*index6pMem & 0x0F); //get last 4 bits of memory
                    *index6pDat = value;
                    index6pDat++;
                    index6pMem++;
                    value = (*index6pMem & 0x30); //get first 2 bits of memory
                    value = value | (*index6pDat & 0x3F);//get last 6 bits.......
                    break;
                case 3:
                    value = *index6pDat & 0xC0;
                    value = value | (*index6pMem & 0x3F);
                    *index6pDat = value;
                    break;
                default: return;
            }
        }
            break; //tasks complete - return from function
        case DIRECT:
            index6p = index6pDat;
            break;
        case TO_MEMORY_DIRECT:
        case TO_MEMORY:
            index6p = index6pMem;
            break;
        default: return;
    }
    if (setmode == FROM_MEMORY) return;
    if(index8%4 == 0){//starts at bit0, last 2 bits intact  
        *index6p = (*index6p & 0x3) |  (value << 2);
    }
    else if (index8%4 == 3){//starts at bit 2, first 2 bits intact
        *index6p = (*index6p & 0xC0) | value;
    }
    else if (index8%4 == 2){//starts at bit 4,upper 4 bits of value, first 4 bits intact
        *index6p = (*index6p & 0xF0) | (value >> 2);
        //lower 2 bits of value, last 6 bits intact
        index6p++;
        *index6p = (*index6p & 0x3F) | (value << 6);
    }
    else if (index8%4 == 1){//start at bit 7, upper 2 bits of value, first 6 bits intact
        *index6p = (*index6p & 0xFC) | (value>>4);
        //lower 4 bits of value, last 4 bits intact
        index6p++;
        *index6p = (*index6p & 0xF) | (value<<4);
    }

    if(setmode == TO_MEMORY_DIRECT)
    {
        setDC(channel, DIRECT, value);
    }

}


uint8_t Tlc59401::getDC(TLC_CHANNEL_TYPE channel, bool direct)
{

    TLC_CHANNEL_TYPE index8 = (NUM_TLCS * 16 -1) - channel;
    uint8_t *index6p;
    uint8_t value; 
    if (direct) index6p = tlc_DCData + ((((uint16_t)index8)*3)>>2);
    else  index6p = tlc_DCMemory + ((((uint16_t)index8)*3)>>2);

    uint8_t position = index8%4;
    switch(position)
    {
        case 0:
            value = *index6p & 0xFC;
            break;
        case 1:
            value = *index6p & 0x03;
            index6p++;
            value = value | (*index6p & 0xF0);
            break;
        case 2:
            value = *index6p & 0x0F;
            index6p++;
            value = value | (*index6p & 0x30);
            break;
        case 3:
            value = *index6p & 0x3F;
            break;
        default: 
            value = 0xFF;
            break;
    }
    return value;
    
}

uint8_t tlc_dcModeStart(void)
{
    if (tlc_needXLAT) {
      return 1;
    }

    disable_XLAT_pulses(); // ensure that no latches happen
    clear_XLAT_interrupt(); // (in case this was called right after update)
    tlc_needXLAT = 0;
    for (int i = 0; i<100; i++){
      __asm__("nop\n\t");
    }

    set_pin(VPRG_PORT, VPRG_PIN);
    //pulse_pin(SCLK_PORT, SCLK_PIN);
    for (int i = 0; i<100; i++){
      __asm__("nop\n\t");
    }
    pulse_pin(SCLK_PORT, SCLK_PIN);
    for (int i = 0; i<100; i++){
      __asm__("nop\n\t");
    }
    return 0; // dot correction mode
}

/** Switches back to grayscale mode. */
uint8_t tlc_dcModeStop(void)
{
    //enable_XLAT_pulses();
    //set_XLAT_interrupt();
    //pulse_pin(XLAT_PORT, XLAT_PIN);

    if (tlc_needXLAT) {
        return 1;
    }
    for (int i = 0; i<100; i++){
      __asm__("nop\n\t");
    }
    clear_pin(VPRG_PORT, VPRG_PIN); // back to grayscale mode
  //  firstGSInput = 1;
    return 0;
}



            
/* @} */
#if XERR_ENABLED

/** Checks for shorted/broken LEDs reported by any of the TLCs.
    \returns 1 if a TLC is reporting an error, 0 otherwise. */
uint8_t Tlc59401::readXERR(void)
{
    return ((XERR_PINS & _BV(XERR_PIN)) == 0);
}

#endif

/* @} */

#if DATA_TRANSFER_MODE == TLC_BITBANG

/** Sets all the bit-bang pins to output */
void tlc_shift8_init(void)
{
    output_pin(SIN_DDR, SIN_PIN);   // SIN as output
    output_pin(SCLK_DDR, SCLK_PIN); // SCLK as output
    clear_pin(SCLK_PORT, SCLK_PIN);
}

/** Shifts a byte out, MSB first */
void tlc_shift8(uint8_t byte)
{
    for (uint8_t bit = 0x80; bit; bit >>= 1) {
        if (bit & byte) {
            set_pin(SIN_PORT, SIN_PIN);
        } else {
            clear_pin(SIN_PORT, SIN_PIN);
        }
        pulse_pin(SCLK_PORT, SCLK_PIN);
    }
}

#elif DATA_TRANSFER_MODE == TLC_SPI

/** Initializes the SPI module to double speed (f_osc / 2) */
void tlc_shift8_init(void)
{
    output_pin(SIN_DDR, SIN_PIN);       // SPI MOSI as output
    output_pin(SCLK_DDR, SCLK_PIN);     // SPI SCK as output
    output_pin(TLC_SS_DDR, TLC_SS_PIN); // SPI SS as output

    clear_pin(SCLK_PORT, SCLK_PIN);

    SPSR = _BV(SPI2X); // double speed (f_osc / 2)
    SPCR = _BV(SPE)    // enable SPI
         | _BV(MSTR);  // master mode
}

/** Shifts out a byte, MSB first */
void tlc_shift8(uint8_t byte)
{
    SPDR = byte; // starts transmission
    while (!(SPSR & _BV(SPIF)))
        ; // wait for transmission complete
}

#endif


/** Preinstantiated Tlc variable. */
Tlc59401 Tlc;

/** \defgroup ExtendedFunctions Extended Library Functions
    These functions require an include statement at the top of the sketch. */
/* @{ */ /* @} */

/** \mainpage
    The <a href="http://www.ti.com/lit/gpn/TLC5940">Texas Instruments TLC5940
    </a> is a 16-channel, constant-current sink LED driver.  Each channel has
    an individually adjustable 4096-step grayscale PWM brightness control and
    a 64-step, constant-current sink (no LED resistors needed!).  This chip
    is a current sink, so be sure to use common anode RGB LEDs.

    Check the <a href="http://code.google.com/p/tlc5940arduino/">tlc5940arduino
    project</a> on Google Code for updates.  To install, unzip the "Tlc5940"
    folder to &lt;Arduino Folder&gt;/hardware/libraries/

    &nbsp;

    \section hardwaresetup Hardware Setup
    The basic hardware setup is explained at the top of the Examples.  A good
    place to start would be the BasicUse Example.  (The examples are in
    File->Sketchbook->Examples->Library-Tlc5940).

    All the options for the library are located in tlc_config.h, including
    #NUM_TLCS, what pins to use, and the PWM period.  After changing
    tlc_config.h, be sure to delete the Tlc5940.o file in the library folder
    to save the changes.

    &nbsp;

    \section libref Library Reference
    \ref CoreFunctions "Core Functions" (see the BasicUse Example and Tlc5940):
    - \link Tlc5940::init Tlc.init(int initialValue (0-4095))\endlink - Call this is
            to setup the timers before using any other Tlc functions.
            initialValue defaults to zero (all channels off).
    - \link Tlc5940::clear Tlc.clear()\endlink - Turns off all channels
            (Needs Tlc.update())
    - \link Tlc5940::set Tlc.set(uint8_t channel (0-(NUM_TLCS * 16 - 1)),
            int value (0-4095))\endlink - sets the grayscale data for channel.
            (Needs Tlc.update())
    - \link Tlc5940::setAll Tlc.setAll(int value(0-4095))\endlink - sets all
            channels to value. (Needs Tlc.update())
    - \link Tlc5940::get uint16_t Tlc.get(uint8_t channel)\endlink - returns
            the grayscale data for channel (see set).
    - \link Tlc5940::update Tlc.update()\endlink - Sends the changes from any
            Tlc.clear's, Tlc.set's, or Tlc.setAll's.

    \ref ExtendedFunctions "Extended Functions".  These require an include
    statement at the top of the sketch to use.

    \ref ReqVPRG_ENABLED "Functions that require VPRG_ENABLED".  These
    require VPRG_ENABLED == 1 in tlc_config.h

    &nbsp;

    \section expansion Expanding the Library
    Lets say we wanted to add a function like "tlc_goCrazy(...)".  The first
    thing to do is to create a source file in the library folder,
    "tlc_my_crazy_functions.h".
    A template for this .h file is
    \code
// Explination for my crazy function extension

#ifndef TLC_MY_CRAZY_FUNCTIONS_H
#define TLC_MY_CRAZY_FUNCTIONS_H

#include "tlc_config.h"
#include "Tlc5940.h"

void tlc_goCrazy(void);

void tlc_goCrazy(void)
{
    uint16_t crazyFactor = 4000;
    Tlc.clear();
    for (uint8_t channel = 4; channel < 9; channel++) {
        Tlc.set(channel, crazyFactor);
    }
    Tlc.update();
}

#endif
 * \endcode
 * Now to use your library in a sketch, just add
 * \code
#include "tlc_my_crazy_functions.h"

// ...

tlc_goCrazy();
    \endcode
    If you would like to share your extended functions for others to use,
    email me (acleone ~AT~ gmail.com) with the file and an example and I'll
    include them in the library.

    &nbsp;

    \section bugs Contact
    If you found a bug in the library, email me so I can fix it!
    My email is acleone ~AT~ gmail.com

    &nbsp;

    \section license License - GPLv3
    Copyright (c) 2009 by Alex Leone <acleone ~AT~ gmail.com>

    This file is part of the Arduino TLC5940 Library.

    The Arduino TLC5940 Library is free software: you can redistribute it
    and/or modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    The Arduino TLC5940 Library is distributed in the hope that it will be
    useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with The Arduino TLC5940 Library.  If not, see
    <http://www.gnu.org/licenses/>. */
