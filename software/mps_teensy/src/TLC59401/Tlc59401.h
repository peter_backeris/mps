/*  Copyright (c) 2009 by Alex Leone <acleone ~AT~ gmail.com>

    This file is part of the Arduino TLC5940 Library.

    The Arduino TLC5940 Library is free software: you can redistribute it
    and/or modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    The Arduino TLC5940 Library is distributed in the hope that it will be
    useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with The Arduino TLC5940 Library.  If not, see
    <http://www.gnu.org/licenses/>. */

#ifndef TLC5940_H
#define TLC5940_H

/** \file
    Tlc5940 library header file. */

#include <Arduino.h>
#include "tlc_config.h"

#if defined(__AVR__)
#ifdef TLC_ATMEGA_8_H

/** Enables the Timer1 Overflow interrupt, which will fire after an XLAT
    pulse */
#define set_XLAT_interrupt()    TIFR |= _BV(TOV1); TIMSK = _BV(TOIE1)
/** Disables any Timer1 interrupts */
#define clear_XLAT_interrupt()  TIMSK = 0

#else

/** Enables the Timer1 Overflow interrupt, which will fire after an XLAT
    pulse */
#define set_XLAT_interrupt()    TIFR1 |= _BV(TOV1); TIMSK1 = _BV(TOIE1)
/** Disables any Timer1 interrupts */
#define clear_XLAT_interrupt()  TIMSK1 = 0

#endif

/** Enables the output of XLAT pulses */
#define enable_XLAT_pulses()    TCCR1A = _BV(COM1A1) | _BV(COM1B1)
/** Disables the output of XLAT pulses */
#define disable_XLAT_pulses()   TCCR1A = _BV(COM1B1)

#elif defined(__arm__) && defined(TEENSYDUINO)
#define set_XLAT_interrupt()    FTM1_SC = FTM_SC_CLKS(1) | FTM_SC_CPWMS | FTM_SC_TOIE | (FTM1_SC & FTM_SC_PS(7))
#define clear_XLAT_interrupt()  FTM1_SC = FTM_SC_CLKS(1) | FTM_SC_CPWMS | (FTM1_SC & FTM_SC_PS(7))
#define enable_XLAT_pulses()    CORE_PIN3_CONFIG = PORT_PCR_MUX(3)|PORT_PCR_DSE|PORT_PCR_SRE
#define disable_XLAT_pulses()   CORE_PIN3_CONFIG = PORT_PCR_MUX(1)|PORT_PCR_DSE|PORT_PCR_SRE

#endif


typedef enum {
    TO_MEMORY,
    FROM_MEMORY,
    DIRECT,
    TO_MEMORY_DIRECT
}set_mode; 



extern volatile uint8_t tlc_needXLAT;
extern volatile void (*tlc_onUpdateFinished)(void);
extern uint8_t tlc_GSData[NUM_TLCS * 24];
extern uint8_t tlc_DCData[NUM_TLCS * 12];
/** The main Tlc5940 class for the entire library.  An instance of this class
    will be preinstantiated as Tlc. */
class Tlc59401
{
  public:
    void init(uint16_t initialValue = 0);
    void clearGS(void);
    void clearGSmem(void);
    uint8_t updateGS(void);
    void setGS(TLC_CHANNEL_TYPE channel, set_mode setmode, uint16_t value);
    uint16_t getGS(TLC_CHANNEL_TYPE channel, bool direct);
    
    void setAllGS(uint16_t value, bool memory);
    void setAllDC(uint8_t value, bool memory);
    void setDC(uint8_t channel, set_mode setmode, uint8_t value);
    uint8_t getDC(uint8_t channel, bool direct);
    void clearDC(void);
    void clearDCmem(void);
    void updateDC(void);
    void restoreAllGS(void);
    void restoreAllDC(void);

#if XERR_ENABLED
    uint8_t readXERR(void);
#endif

};

uint8_t tlc_dcModeStart(void);
uint8_t tlc_dcModeStop(void);
void tlc_shift8_init(void);
void tlc_shift8(uint8_t byte);


// for the preinstantiated Tlc variable.
extern Tlc59401 Tlc;

#endif
