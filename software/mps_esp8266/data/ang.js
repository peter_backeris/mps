var inputDirectField = document.getElementById('inputDirectField');
var receiveText = document.getElementById('receiveText');
var selectExp = document.getElementById('selectExp');
var resultTable = document.getElementById('resultTable');

function init() {
  var wsUri = 'ws://' + window.location.hostname + ':81/';

  websocket = new WebSocket(wsUri);
  websocket.onopen = function (evt) { onOpen(evt); };
  websocket.onclose = function (evt) { onClose(evt); };
  websocket.onerror = function (evt) { onError(evt); };
  websocket.onmessage = function (evt) { onMessage(evt); };
}

function onOpen() {
  console.log('CONNECTED');
  websocket.send("$r");
}

function onClose() {
  console.log("CLOSED");
}

function onError(evt) {
  var obj = JSON.parse(evt.error);
  console.log(obj.data);
}

function onMessage(evt) {
  evt.preventDefault();
  console.log(evt.data);
  //var obj = isJSON(evt.data) ? JSON.parse(evt.data) : evt.data;
  var obj = JSON.parse(evt.data);
  console.log(obj);
  /*
  if (obj && !obj.case) {
    insertMessage(obj);
    console.log("not json")
    return;
  }
  */

  //console.log(obj.case);

  switch(obj.case) {
    case 0:
      // Insert text
      insertMessage(obj.data);

      break;
    case 1:
      // Clear all options
      clearUploadedList();

      // Insert options in dropdown
      for (var i = 0; i < obj.files.length; i++) {
        selectExp.options[selectExp.options.length] = new Option(obj.files[i], obj.files[i]);
      }

      break;
    case 2:
      clearTableRows();

      //Add the data rows.
      for (var j = 0; j < obj.Groups.length; j++) {
        row = resultTable.tBodies[0].insertRow(-1);

        var cell1 = row.insertCell(0);
        cell1.innerHTML = obj.Groups[j].name;

        var cell2 = row.insertCell(1);
        cell2.innerHTML = obj.Groups[j].Channels.join();

        var cell3 = row.insertCell(2);
        cell3.innerHTML = obj.Groups[j].irradiance;

        var cell4 = row.insertCell(3);
        cell4.innerHTML = obj.Groups[j].duration;

        var cell5 = row.insertCell(4);
        cell5.innerHTML = obj.Groups[j].start_time;

        var cell6 = row.insertCell(5);
        cell6.innerHTML = obj.Groups[j].cycles;

        var cell7 = row.insertCell(6);
        cell7.innerHTML = obj.Groups[j].offtime;
      }

      break;
  }
}

// Check if string is json stringify of simple string
function isJSON(data) {
  var ret = true;
  try {
    JSON.parse(data);
  }catch(e) {
    ret = false;
  }
  return ret;
}

// Clear result table tbody
function clearTableRows() {
  var rowCount = resultTable.rows.length;
  for (var x=rowCount-1; x>0; x--) {
    resultTable.deleteRow(x);
  }
}

// Remove one / clear all dropdown list (with files names)
function clearUploadedList() {
  // Remove one option
  while (selectExp.options.length > 0) {
      selectExp.remove(0);
  }
}

//view selected experiment
function viewExperiment(e){
  e.preventDefault();
  var val = selectExp.value;
  websocket.send("$v" + val);
  console.log("Request table view of program " + val);

}


// Send text manually from text input
function sendSocketManually(e) {
  e.preventDefault();

  var val = inputDirectField.value;
  if (!val || !val.length) return;
  console.log(val);
  websocket.send("$c" + val);
  inputDirectField.value = '';
}

// Insert message (response) in to result block
function insertMessage(text) {
  console.log(text);
  var div = document.createElement("div");
  div.classList.add('column');
  div.innerHTML = text;
  receiveText.insertBefore(div, receiveText.firstChild);
}

//load experiment from selection
function loadExperiment(e) {
  e.preventDefault();
  var val = selectExp.value;
  websocket.send("$l" + val);
  console.log(val);
}



function stopExperiment() {
  websocket.send('$cx');
}

function startExperiment(){
  websocket.send('$cs');
}

function removeExperiment() {
  var selectedOption = selectExp.options && selectExp.options.length ? selectExp.options[selectExp.selectedIndex].value : null;
  console.log(selectedOption);
  if (selectedOption) {
    // Remove one item from files dropdown
      websocket.send('$d' + selectedOption);
  }
}
