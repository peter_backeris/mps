//mps_ESP8266.ino
//(C)2019 Peter Backeris

#include <ESP8266WiFi.h>
//#include "src\ESP8266Wifi.h"

#include <WiFiClient.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266mDNS.h>
#include <ESP8266WebServer.h>
#include <WebSocketsServer.h>
#include <FS.h>   // Include the SPIFFS library
#include "ArduinoJson.h"

extern "C" {
#include "user_interface.h"
}

static const char ssid[] = "MPS_96-well_428nm"; //ssid name indicating MPS board type and configuration
static const char password[] = "123456789"; //password for access point


ESP8266WiFiMulti wifiMulti;     // Create an instance of the ESP8266WiFiMulti class, called 'wifiMulti'

ESP8266WebServer server(80);    // Create a webserver object that listens for HTTP request on port 80

File fsUploadFile;              // a File object to temporarily store the received file

String getContentType(String filename); // convert the file extension to the MIME type
bool handleFileRead(String path);       // send the right file to the client (if it exists)
void handleFileUpload();                // upload a new file to the SPIFFS

WebSocketsServer webSocket = WebSocketsServer(81);

const int LEDPIN = 2;
// Current LED status
bool LEDStatus;

String response;
char jsontxt[14000]; //char buffer for json files
String jsonstr;
//String response = "";
char c;
int ind = 0;
static void writeLED(bool LEDon);
void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t length);


void setup() {
  pinMode(LEDPIN, OUTPUT);

  writeLED(true);
  Serial.setTimeout(200);
  Serial.begin(115200);         // Start the Serial communication to send messages to the computer
  delay(10);
  Serial.println('x'); //turn off all leds

  WiFi.mode(WIFI_AP);
  WiFi.softAP(ssid, password);
  IPAddress myIP = WiFi.softAPIP();
  // Send the IP address of the ESP8266 to the computer

  char hostname [3];
  strcpy(hostname, "MPS");
  if (MDNS.begin(hostname, WiFi.localIP())) {
    //Serial.println("MDNS responder started");
    MDNS.addService("http", "tcp", 80);
    MDNS.addService("ws", "tcp", 81);
  }
  //else {
  //   Serial.println("MDNS.begin failed");
  //  }
  //  Serial.print("Connect to http://esp8266.local or http://");
  //  Serial.println(WiFi.localIP());`
  SPIFFS.begin();                           // Start the SPI Flash Files System

  // server.on("/upload", HTTP_GET, []() {                 // if the client requests the upload page
  //    if (!handleFileRead("/upload.html"))                // send it if it exists
  //     server.send(404, "text/plain", "404: Not Found"); // otherwise, respond with a 404 (Not Found) error
  // });
  server.on("/", handleRoot);

  server.on("/upload", HTTP_POST, []() {                      // if the client posts to the upload page
    server.send(204);
  },                          // Send status 200 (OK) to tell the client we are ready to receive
  handleFileUpload );                                // Receive and save the file


  server.onNotFound([]() {                              // If the client requests any URI
    if (!handleFileRead(server.uri()))                  // send it if it exists
      server.send(404, "text/plain", "404: Not Found"); // otherwise, respond with a 404 (Not Found) error
  });

  server.begin();                           // Actually start the server
  webSocket.begin();
  webSocket.onEvent(webSocketEvent);
  //Serial.println("HTTP server started");

}

void loop() {
  server.handleClient();
  webSocket.loop();
  if (Serial.available()) {
    //DynamicJsonBuffer jsonBuffer(1000);
    response = Serial.readString();
    broadcastMessage(response);
    /*JsonObject& root = jsonBuffer.createObject();
      root["case"] = 0;
      root["data"] = response;
      root.printTo((char*)jsontxt, root.measureLength() + 1);

      webSocket.broadcastTXT(jsontxt, root.measureLength());
    */
    //Serial.println(jsontxt);

  }
}



//send message to WS client
static void broadcastMessage(String message) {
  DynamicJsonBuffer jsonBuffer(10000);
  JsonObject& root = jsonBuffer.createObject();
  root["case"] = 0;        //defines that this is standard message data
  root["data"] = message; //message contents
  root.printTo((char*)jsontxt, root.measureLength() + 1); //serialize json

  webSocket.broadcastTXT(jsontxt, root.measureLength()); //broadcast json contents over ws server

}



String getContentType(String filename) { // convert the file extension to the MIME type
  if (filename.endsWith(".html")) return "text/html";
  else if (filename.endsWith(".css")) return "text/css";
  else if (filename.endsWith(".js")) return "application/javascript";
  else if (filename.endsWith(".ico")) return "image/x-icon";
  else if (filename.endsWith(".gz")) return "application/x-gzip";
  return "text/plain";
}

bool handleFileRead(String path) { // send the right file to the client (if it exists)
  //Serial.println("handleFileRead: " + path);
  if (path.endsWith("/")) path += "index.html";          // If a folder is requested, send the index file
  String contentType = getContentType(path);             // Get the MIME type
  String pathWithGz = path + ".gz";
  if (SPIFFS.exists(pathWithGz) || SPIFFS.exists(path)) { // If the file exists, either as a compressed archive, or normal
    if (SPIFFS.exists(pathWithGz))                         // If there's a compressed version available
      path += ".gz";                                         // Use the compressed verion
    File file = SPIFFS.open(path, "r");                    // Open the file
    size_t sent = server.streamFile(file, contentType);    // Send it to the client
    file.close();                                          // Close the file again
    // Serial.println(String("\tSent file: ") + path);
    return true;
  }
  //Serial.println(String("\tFile Not Found: ") + path);   // If the file doesn't exist, return false
  return false;
}





void handleRoot()
{
  if (!handleFileRead("/index.html")) {               // send it if it exists
    server.send(404, "text/plain", "404: Not Found"); // otherwise, respond with a 404 (Not Found) error
  }
}



/* Debugging functions - not needed
  void printDir() {
  Dir dir = SPIFFS.openDir("/");
  while (dir.next()) {
    //Serial.print(dir.fileName());
    File f = dir.openFile("r");
    //Serial.println(f.size());
  }
  }

*/


static void writeLED(bool LEDon)
{
  LEDStatus = LEDon;
  // Note inverted logic for Adafruit HUZZAH board
  if (LEDon) {
    digitalWrite(LEDPIN, 0);
  }
  else {
    digitalWrite(LEDPIN, 1);
  }
}


//update file selection dropdown on
void updateSelectOptions() {
  DynamicJsonBuffer jsonBuffer(1000);
  JsonObject& root = jsonBuffer.createObject();
  JsonArray& files = root.createNestedArray("files");
  root["case"] = 1; //defines data to be select options data

  Dir dir = SPIFFS.openDir("/");
  int i = 0;
  while (dir.next() & i < 10) {
    if (dir.fileName().endsWith(".json")) {
      files.add(dir.fileName());
      //broadcastMessage(dir.fileName());
      //broadcastMessage(String(i));
      i++;
    }
  }
  //root.prettyPrintTo(Serial);
  root.printTo((char*)jsontxt, root.measureLength() + 1);

  webSocket.broadcastTXT(jsontxt, root.measureLength());
}


bool loadExperiment(char* filename, bool runExp = false) { //view on client and/or load json experiment to Teensy
  DynamicJsonBuffer jsonBuffer(10000);
  //uint32_t hfree = system_get_free_heap_size();
  //hfree = system_get_free_heap_size();
  //Serial.print("Free heap size ");
  //Serial.println(free);
  File jsonFile = SPIFFS.open(filename, "r"); //open file for reading
  //broadcastMessage("Heap Size = ");
  //broadcastMessage((String)hfree);
  if (!jsonFile) {
    //Serial.println("Failed to open json file");
    broadcastMessage("Failed to open json file");

    return false;
  }

  size_t jsize = jsonFile.size();
  /*
    Serial.print("Jsonfile size: " );
    Serial.println((String) jsize);
  */
  /*
    if (jsize > hfree) {
    // Serial.println(F("Json file size is too large"));
    broadcastMessage(F("Json file is too large"));
    return false;
    }
  */
  /*
    // Allocate a buffer to store contents of the file.
    std::unique_ptr<char[]> buf(new char[jsize]);
    // We don't use String here because ArduinoJson library requires the input
    // buffer to be mutable. If you don't use ArduinoJson, you may as well
    // use configFile.readString instead.
    jsonFile.readBytes(buf.get(), jsize);
    JsonObject& json = jsonBuffer.parseObject(buf.get());
    Serial.print("jsonbuffer size: ");
    Serial.println(jsonBuffer.size());
    if (!json.success()) {
      //Serial.println("Failed to parse json file");
      broadcastMessage("Failed to parse json file");
      return false;
    }
  */

  JsonObject& json = jsonBuffer.parseObject(jsonFile);

  json["case"] = 2;
  /*json.prettyPrintTo(Serial);

    Serial.print("json size ");
    Serial.println(json.measureLength());
  */

  json.printTo((char*)jsontxt, json.measureLength() + 1);

  //Serial.println("Sending json view");
  delay(200);
  webSocket.broadcastTXT(jsontxt, json.measureLength());
  delay(200);
  //broadcastMessage(jsontxt);
  //webSocket.broadcastTXT(jsontxt, json.measureLength());
  //hfree = system_get_free_heap_size();
  // Serial.println((String)hfree);
  // broadcastMessage("Heap Size = ");
  // broadcastMessage((String)hfree);
  //response = String(jsontxt);
  //broadcastMessage(response);
  //uint32_t free = system_get_free_heap_size();
  //free = system_get_free_heap_size();
  //Serial.print(F("Free heap size "));
  // Serial.println(free);
  // Serial.println(F("Number of groups "));
  //Serial.println(json["Groups"].size());

  // load experiment onto Teensy if specified
  if (runExp) {
    Serial.print('n'); Serial.println(json["Groups"].size());
    int tmp = 0;
    int nchan;
    int offtime;
    int duration;
    int timeout = millis();
    while (!Serial.available() & tmp < 2000) {
      tmp = millis() - timeout;
    }
    if (Serial.available()) {
      response = Serial.readString();
      broadcastMessage(response);
    }
    else
    {
      broadcastMessage("Error communicating with microcontroller.");
    }
    delay(300);
    for (int i = 0; i < json["Groups"].size(); i++) {
      String gname = json["Groups"][i]["name"];
      String gparams = String("g");
      gparams += String(i + 1);
     
    
      if (((float)json["Groups"][i]["duration"] < 1 && (float)json["Groups"][i]["duration"] > 0 ) || ((float)json["Groups"][i]["offtime"] < 1 && (float)json["Groups"][i]["offtime"]>0)) {
        
        nchan = -(json["Groups"][i]["Channels"].size());
        duration = round((float)(json["Groups"][i]["duration"]) * 1000);
        offtime = round((float)(json["Groups"][i]["offtime"]) * 1000);
     
      }
      else {
        nchan = json["Groups"][i]["Channels"].size();
        duration = json["Groups"][i]["duration"];
        offtime = json["Groups"][i]["offtime"];
        
      }

      gparams += ",";
      gparams += String(nchan);
      gparams += ",";
      int tmp;
      for (int j = 0; j < abs(nchan); j++) {

        tmp = json["Groups"][i]["Channels"][j];
        gparams += String(tmp);
        gparams += ",";
      }
      tmp = json["Groups"][i]["irradiance"];
      //tmp = tmp1 * 100;

      gparams += String(tmp);
      gparams += ",";
   
      gparams += String(duration);
      gparams += ",";
      tmp = json["Groups"][i]["start_time"];
      gparams += String(tmp);
      gparams += ",";
     
      gparams += String(offtime);
      gparams += ",";
      tmp = json["Groups"][i]["cycles"];
      gparams += String(tmp);
      Serial.println(gparams);
      tmp = 0;
      timeout = millis();
      while (!Serial.available() & tmp < 2000) {
        tmp = millis() - timeout;
      }

      delay(300);
      if (Serial.available()) {

        response = Serial.readString();
        broadcastMessage(response);
        delay(300);

      }

      else {
        response = String("Communication error with microcontroller");
        broadcastMessage(response);
        return false;
      }



    }
    Serial.println("i");
    delay(300);
    if (Serial.available()) {

      response = Serial.readString();
      broadcastMessage(response);

    }

  }

  return true;
}




void handleFileUpload() { // upload a new file to the SPIFFS
  HTTPUpload& upload = server.upload();
  broadcastMessage("Uploading file");
  if (upload.status == UPLOAD_FILE_START) {
    String filename = upload.filename;
    if (!filename.startsWith("/")) filename = "/" + filename;
    // Serial.print("handleFileUpload Name: "); Serial.println(filename);
    fsUploadFile = SPIFFS.open(filename, "w");            // Open the file for writing in SPIFFS (create if it doesn't exist)
    filename = String();
  } else if (upload.status == UPLOAD_FILE_WRITE) {
    if (fsUploadFile)
      fsUploadFile.write(upload.buf, upload.currentSize); // Write the received bytes to the file
  } else if (upload.status == UPLOAD_FILE_END) {
    if (fsUploadFile) {                                   // If the file was successfully created
      fsUploadFile.close();                               // Close the file again
      broadcastMessage("handleFileUpload Size: "); broadcastMessage(String(upload.totalSize));
      //server.sendHeader("Location","/success.html");      // Redirect the client to the success page
      //server.send(303);
      updateSelectOptions();

    }

  }
  else {
    server.send(500, "text/plain", "500: couldn't create file");
  }
}


void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t length)
{
  //Serial.printf("webSocketEvent(%d, %d, ...)\r\n", num, type);
  switch (type) {
    case WStype_DISCONNECTED:
      //  Serial.printf("[%u] Disconnected!\r\n", num);
      break;
    case WStype_CONNECTED:
      {
        IPAddress ip = webSocket.remoteIP(num);
        //Serial.printf("[%u] Connected from %d.%d.%d.%d url: %s\r\n", num, ip[0], ip[1], ip[2], ip[3], payload);
        // Send the current LED status

      }
      break;
    case WStype_TEXT:
      if (payload[0] == '$') {
        char c = payload[1];
        payload += 2;
        switch (c) {

          case ('r'):
            {
              updateSelectOptions();
              break;
            }


          case ('v'):

            {
              //Serial.print("Loading Experiment ");

              String message = String("Viewing Experiment ");

              message += String((char*)payload) ;
              broadcastMessage(message);


              //Serial.printf("%s\r\n", payload );
              loadExperiment((char*)payload, false);
              break;
            }

          case ('l'):
            {
              //Serial.print("Loading Experiment ");

              String message = String("Loading Experiment ");

              message += String((char*)payload) ;
              broadcastMessage(message);


              //Serial.printf("%s\r\n", payload );
              loadExperiment((char*)payload, true);
              break;
            }
          case ('d'):

            {
              String message = String("Deleting Experiment " );
              message += String((char*)payload);
              broadcastMessage(message);
              if (SPIFFS.remove((char*)payload)) {
                broadcastMessage("Success");
              }
              updateSelectOptions();
              break;
            }

          case ('c'):
            Serial.write(payload, length - 2);
            Serial.println();

        }
      }

      /*(
        Serial.printf("[%u] get Text: %s\r\n", num, payload);
        Serial.write(payload, length);
        Serial.println();
      */

      // send data to all connected clients
      //webSocket.broadcastTXT(payload, length);
      break;
    case WStype_BIN:
      //Serial.printf("[%u] get binary length: %u\r\n", num, length);
      hexdump(payload, length);

      // echo data back to browser
      webSocket.sendBIN(num, payload, length);
      break;
    default:
      //  Serial.printf("Invalid WStype [%d]\r\n", type);

      break;
  }
}
