EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:eeschema_symbols
LIBS:MPS
LIBS:TLC59401_Breakout-cache
EELAYER 25 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 3400 2050 2    60   ~ 0
IREF1
Text Label 3400 1950 2    60   ~ 0
VDRV
Text Label 3400 2150 2    60   ~ 0
VDRV
Text Label 1700 1950 0    60   ~ 0
GND
Text Label 1700 2050 0    60   ~ 0
BLANK
Text Label 1700 2150 0    60   ~ 0
XLAT
Text Label 1700 2250 0    60   ~ 0
SCLK
Text Label 1700 2350 0    60   ~ 0
SIN
Text Label 1700 2450 0    60   ~ 0
MODE
Text Label 3400 2250 2    60   ~ 0
GSCLK
Text Label 3400 2350 2    60   ~ 0
SOUT
Text Label 3400 2450 2    60   ~ 0
XERR
$Comp
L TLC59401_HTSSOP TLC59401
U 1 1 57387831
P 2550 2600
F 0 "TLC59401" H 2550 3650 118 0000 C CNN
F 1 "TLC59401_HTSSOP" H 2550 3450 118 0000 C CNN
F 2 "footprints:TexasInstruments_PWP_R-PDSO-G28" H 2550 2600 118 0001 C CNN
F 3 "" H 2550 2600 118 0000 C CNN
	1    2550 2600
	1    0    0    -1  
$EndComp
$Comp
L C C1
U 1 1 573AEAA8
P 1150 1750
F 0 "C1" H 1175 1850 50  0000 L CNN
F 1 "100n" H 1175 1650 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 1188 1600 50  0001 C CNN
F 3 "" H 1150 1750 50  0000 C CNN
	1    1150 1750
	-1   0    0    1   
$EndComp
Text Notes 850  1250 0    60   ~ 0
bypass capacitors
Text Notes 3950 1550 0    60   ~ 0
current setting resistors\n1k5 = 26mA max
$Comp
L R Riref1
U 1 1 573AFACB
P 4650 1850
F 0 "Riref1" V 4730 1850 50  0000 C CNN
F 1 "1k5" V 4650 1850 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 4580 1850 50  0001 C CNN
F 3 "" H 4650 1850 50  0000 C CNN
	1    4650 1850
	0    -1   -1   0   
$EndComp
Text Label 4150 1850 0    60   ~ 0
IREF1
Text Notes 12250 10950 0    79   ~ 0
TLC59401 Breakout Board\n
Text Notes 11900 10450 0    79   ~ 0
Copyright (c) 2016 Suzanna Katz, Peter Backeris, Chris Merck\n
Text Notes 1950 1300 0    60   ~ 0
constant-current drivers\n12-bit PWM,  7-bit current control
Text Label 1700 3250 0    60   ~ 0
D8
Text Label 1700 3150 0    60   ~ 0
D7
Text Label 1700 3050 0    60   ~ 0
D6
Text Label 1700 2950 0    60   ~ 0
D5
Text Label 1700 2850 0    60   ~ 0
D4
Text Label 1700 2750 0    60   ~ 0
D3
Text Label 1700 2650 0    60   ~ 0
D2
Text Label 1700 2550 0    60   ~ 0
D1
Text Label 3400 3250 2    60   ~ 0
D9
Text Label 3400 3150 2    60   ~ 0
D10
Text Label 3400 3050 2    60   ~ 0
D11
Text Label 3400 2950 2    60   ~ 0
D12
Text Label 3400 2850 2    60   ~ 0
D13
Text Label 3400 2750 2    60   ~ 0
D14
Text Label 3400 2650 2    60   ~ 0
D15
Text Label 3400 2550 2    60   ~ 0
D16
Wire Wire Line
	3050 2050 3400 2050
Wire Wire Line
	3050 1950 3400 1950
Wire Wire Line
	3050 2150 3400 2150
Wire Wire Line
	2050 1950 1700 1950
Wire Wire Line
	2050 2050 1700 2050
Wire Wire Line
	2050 2150 1700 2150
Wire Wire Line
	2050 2250 1700 2250
Wire Wire Line
	2050 2450 1700 2450
Wire Wire Line
	3050 2250 3400 2250
Wire Wire Line
	3050 2350 3400 2350
Wire Wire Line
	3050 2450 3400 2450
Wire Wire Line
	1700 2350 2050 2350
Wire Wire Line
	4500 1850 4150 1850
Text Label 1150 1450 0    60   ~ 0
VDRV
Wire Wire Line
	1150 1450 1150 1600
Text Label 5000 1850 0    60   ~ 0
GND
Text Label 1150 2000 0    60   ~ 0
GND
Wire Wire Line
	4800 1850 5000 1850
Wire Wire Line
	1150 1900 1150 2000
Wire Wire Line
	1700 2550 2050 2550
Wire Wire Line
	1700 2650 2050 2650
Wire Wire Line
	1700 2750 2050 2750
Wire Wire Line
	1700 2850 2050 2850
Wire Wire Line
	1700 2950 2050 2950
Wire Wire Line
	1700 3050 2050 3050
Wire Wire Line
	1700 3150 2050 3150
Wire Wire Line
	1700 3250 2050 3250
Wire Wire Line
	3050 2550 3400 2550
Wire Wire Line
	3050 2650 3400 2650
Wire Wire Line
	3050 2750 3400 2750
Wire Wire Line
	3050 2850 3400 2850
Wire Wire Line
	3400 2950 3050 2950
Wire Wire Line
	3050 3050 3400 3050
Wire Wire Line
	3050 3150 3400 3150
Wire Wire Line
	3050 3250 3400 3250
Text Label 2000 3800 0    60   ~ 0
GND
Text Label 2000 3900 0    60   ~ 0
BLANK
Text Label 2000 4000 0    60   ~ 0
XLAT
Text Label 2000 4100 0    60   ~ 0
SCLK
Text Label 2000 4200 0    60   ~ 0
SIN
Text Label 2000 4300 0    60   ~ 0
MODE
Text Label 2000 5100 0    60   ~ 0
D8
Text Label 2000 5000 0    60   ~ 0
D7
Text Label 2000 4900 0    60   ~ 0
D6
Text Label 2000 4800 0    60   ~ 0
D5
Text Label 2000 4700 0    60   ~ 0
D4
Text Label 2000 4600 0    60   ~ 0
D3
Text Label 2000 4500 0    60   ~ 0
D2
Text Label 2000 4400 0    60   ~ 0
D1
Wire Wire Line
	2350 3800 2000 3800
Wire Wire Line
	2350 3900 2000 3900
Wire Wire Line
	2350 4000 2000 4000
Wire Wire Line
	2350 4100 2000 4100
Wire Wire Line
	2350 4300 2000 4300
Wire Wire Line
	2000 4200 2350 4200
Wire Wire Line
	2000 4400 2350 4400
Wire Wire Line
	2000 4500 2350 4500
Wire Wire Line
	2000 4600 2350 4600
Wire Wire Line
	2000 4700 2350 4700
Wire Wire Line
	2000 4800 2350 4800
Wire Wire Line
	2000 4900 2350 4900
Wire Wire Line
	2000 5000 2350 5000
Wire Wire Line
	2000 5100 2350 5100
Text Label 3400 3900 2    60   ~ 0
IREF1
Text Label 3400 3800 2    60   ~ 0
VDRV
Text Label 3400 4000 2    60   ~ 0
VDRV
Text Label 3400 4100 2    60   ~ 0
GSCLK
Text Label 3400 4200 2    60   ~ 0
SOUT
Text Label 3400 4300 2    60   ~ 0
XERR
Text Label 3400 5100 2    60   ~ 0
D9
Text Label 3400 5000 2    60   ~ 0
D10
Text Label 3400 4900 2    60   ~ 0
D11
Text Label 3400 4800 2    60   ~ 0
D12
Text Label 3400 4700 2    60   ~ 0
D13
Text Label 3400 4600 2    60   ~ 0
D14
Text Label 3400 4500 2    60   ~ 0
D15
Text Label 3400 4400 2    60   ~ 0
D16
Wire Wire Line
	3050 3900 3400 3900
Wire Wire Line
	3050 3800 3400 3800
Wire Wire Line
	3050 4000 3400 4000
Wire Wire Line
	3050 4100 3400 4100
Wire Wire Line
	3050 4200 3400 4200
Wire Wire Line
	3050 4300 3400 4300
Wire Wire Line
	3050 4400 3400 4400
Wire Wire Line
	3050 4500 3400 4500
Wire Wire Line
	3050 4600 3400 4600
Wire Wire Line
	3050 4700 3400 4700
Wire Wire Line
	3400 4800 3050 4800
Wire Wire Line
	3050 4900 3400 4900
Wire Wire Line
	3050 5000 3400 5000
Wire Wire Line
	3050 5100 3400 5100
$Comp
L TLC_BREAKOUT P1
U 1 1 5B09DCEE
P 2700 4350
F 0 "P1" H 2700 4450 50  0000 C CNN
F 1 "TLC_BREAKOUT" H 2700 5050 50  0000 C CNN
F 2 "MPS:TLC_BREAKOUT_v2" H 2700 4350 50  0000 C CNN
F 3 "" H 2700 4350 50  0000 C CNN
	1    2700 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	2350 5200 2000 5200
Wire Wire Line
	3050 5200 3400 5200
Text Label 2000 5200 0    60   ~ 0
GND
Text Label 3400 5200 2    60   ~ 0
GND
$EndSCHEMATC
