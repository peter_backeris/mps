# Welcome

Welcome to the repository for the Microplate Photoirradiation System (MPS). 

## Contents

The MPS repository contains 4 main folders:

* hardware - contains all PCB design files for KiCAD, dxf files for mechanical components (for laser cutter)
* software - contains code for all firmware and user applications\
* documentation - contains pdf of user guide and any other supporting documentation
* Experimental Data Repo - contains raw data for validation experiments for the original[ PLOS ONE Publication
](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0203597)


## Software Setup
[Firmware Installation](https://bitbucket.org/peter_backeris/mps/wiki/Software/Firmware%20Installation)


[MPS Calibration Utility Setup](https://bitbucket.org/peter_backeris/mps/wiki/MPS%20Calibration%20Utility%20-%20Setup%20Instructions)




## Supplementary Documentation

Additional instructions and information will be added to the Wiki as needed to help users understand all aspects of design and fabrication fully.

[Bill of Materials (electrical)](https://docs.google.com/spreadsheets/d/1GQ3c3pxxSYDxqRmBIwMVoEM9ko7vX5CvQuASelmT4HQ/edit?usp=sharing)

[Choosing Current-Setting Resistors (R_iref)](https://bitbucket.org/peter_backeris/mps/wiki/Choosing%20Current-Setting%20Resistors%20(R_iref))

[MPS Calibration Utility User Guide](https://bitbucket.org/peter_backeris/mps/wiki/MPS%20Calibration%20Utility%20-%20Usage)